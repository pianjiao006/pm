# 叮叮智能saas运营平台

#### 介绍
叮叮智能saas运营平台基于Spring Boot2.0、Spring Security、Vue、Redis、tkmapper的前后端分离saas管理平台，前端主要是依赖若依来展示，后端mapper改为tk.mapper。
- 特别鸣谢：[element](https://gitee.com/ElemeFE/element)，[vue-element-admin](https://gitee.com/mirrors/vue-element-admin)，[ruoyi-vue](https://gitee.com/y_project/RuoYi-Vue)，[通用mapper](https://gitee.com/free/Mapper)

#### 软件架构
- 后端架构基于SpringBoot2.0开发
- 权限控制：Spring Security和jwt
- 缓存：redis
- 数据库：mysql5.7
- 集成Mybatis通用Mapper: tk.mapper
- 前端：Vue

#### 系统功能介绍
- pm-common 存放所有其他模块公共的类 例如工具类、基础配置类、基础model
- pm-generator 表生成代码
- pm-job 任务执行模块
- pm-sys 系统基础模块
   - 租户管理
   - 权限管理
   - 参数配置
   - 字典管理
   - 日志管理

#### 安装教程
- 后端启动：
1.  git clone https://gitee.com/pianjiao006/pm.git
2.  导入IDE（idea或者eclipse）
3.  预先导入数据库文件，接着启动好Redis、mysql，并在application.yml文件修改配置
4.  在pm-sys-web-start下面找到SysWebApplication的main方法可以启动

- 前端启动：
1.  进入pm-vue目录
2.  npm install (建议使用阿里镜像:  [具体可参考](https://my.oschina.net/u/1156250/blog/5011842))
3.  安装完毕，然后启动 npm run dev
4.  访问http://localhost

#### 更新安排
1.  集成阿里云oss （已经集成，配置相关的阿里云oss配置可以使用）
2.  集成docker
3.  集成分布式文件系统
4.  集成MQ: RabbitMq

#### 使用说明

1.  登陆
![登陆](https://images.gitee.com/uploads/images/2021/0617/162555_7bd3e248_359263.png "屏幕截图.png")
2.  主控制台
![输入图片说明](https://images.gitee.com/uploads/images/2021/0617/162722_b4b7b9ca_359263.png "屏幕截图.png")
3. 租户
![输入图片说明](https://images.gitee.com/uploads/images/2021/0621/162523_f0f44df3_359263.png "屏幕截图.png")
   
#### generator
1.  找到GenFileServiceTest进入genFiles方法
2.  在genFiles方法设置前后端文件输出路径，一般两种情况： 
    - 直接输入到工程指定路径（单工程）
    - 输出到某个路径然后再拷贝到工程下面（多工程）
3.  输入表名，支持多个表（用逗号分开即可）
4.  执行单元测试genFiles方法即可生成文件

已有基于此平台开发的产品
-  ![叮叮趣约](https://images.gitee.com/uploads/images/2021/0820/113237_8aaa6514_359263.jpeg "ddqy-logo.jpg") 
[叮叮趣约](https://gitee.com/maliwuli_0/huiyishiyuyue)
   
#### 部署
后端项目：
1. 在根目录执行：mvn clean package -Dmaven.skip.test=true
2. 获取pm-sys/pm-sys-web-start/target下面的jar包，然后执行java -jar pm-sys-web-start.jar &

前端项目：
1. 打包正式环境，执行npm run build:prod
2. 构建打包成功之后，会在根目录生成 dist 文件夹，里面就是构建打包好的文件，通常是 ***.js 、***.css、index.html 等静态文件。
    通常情况下 dist 文件夹的静态文件发布到你的 nginx 或者静态服务器即可，其中的 index.html 是后台服务的入口页面。
3. 使用Nginx代理映射静态文件 
~~~
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;
    keepalive_timeout  65;
    server {
        listen       80;
        server_name  localhost;
		charset utf-8;

		location / {
            root   /home/ruoyi/projects/ruoyi-ui;
			try_files $uri $uri/ /index.html;
            index  index.html index.htm;
        }
		
		location /prod-api/{
			proxy_set_header Host $http_host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header REMOTE-HOST $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_pass http://localhost:8080/;
		}

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }
}
~~~

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 交流
欢迎小伙伴入群聊聊，群号：774656614 <br/>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0621/160913_92ec39a8_359263.png "屏幕截图.png")

### 还有其他系统开发中，诚邀前端小伙伴，可以加入上面群联系群主

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

package com.pimee.common.db.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.pimee.common.constants.Constants.DataSourceType;

/**
 * 自定义多数据源切换注解
 * 
 * @description
 * @author Bruce Shaw 2020年2月9日 下午3:54:49
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DataSource {
	/**
	 * 切换数据源名称
	 */
	DataSourceType value() default DataSourceType.MASTER;
}

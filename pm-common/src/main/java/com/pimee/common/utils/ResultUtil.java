package com.pimee.common.utils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.pimee.common.core.support.HttpCode;

public class ResultUtil {
	/**
	 * 成功结果提示 不带数据
	 * 
	 * @return
	 */
	public static Object ok() {
		ModelMap modelMap = new ModelMap();
		modelMap.put("code", HttpCode.OK.value());
		modelMap.put("msg", HttpCode.OK.msg());
		modelMap.put("timestamp", System.currentTimeMillis());
		return modelMap;
	}
	
	public static Object error() {
		ModelMap modelMap = new ModelMap();
		modelMap.put("code", HttpCode.INTERNAL_SERVER_ERROR.value());
		modelMap.put("msg", HttpCode.INTERNAL_SERVER_ERROR.msg());
		modelMap.put("timestamp", System.currentTimeMillis());
		return modelMap;
	}
	
	public static Object error(String msg) {
		ModelMap modelMap = new ModelMap();
		modelMap.put("code", HttpCode.INTERNAL_SERVER_ERROR.value());
		modelMap.put("msg", msg);
		modelMap.put("timestamp", System.currentTimeMillis());
		return modelMap;
	}

	/**
	 * 扩展原结果
	 * 
	 * @param param
	 * @return
	 */
	public static Object okMap(Map<String, Object> param) {
		Map<String, Object> map = Maps.newLinkedHashMap();
		map.putAll(param);
		map.put("code", HttpCode.OK.value());
		map.put("msg", HttpCode.OK.msg());
		map.put("timestamp", System.currentTimeMillis());
		return map;
	}

	/**
	 * 分页列表
	 * @param list
	 * @return
	 */
	public static Object okPageList(List<?> list){
		Map<String, Object> map = Maps.newLinkedHashMap();
		map.put("code", HttpCode.OK.value());
		map.put("msg", HttpCode.OK.msg());
		map.put("rows", list);
		map.put("total", new PageInfo(list).getTotal());
		return map;
	}

	/** 设置成功响应代码 */
	public static Object ok(Object data) {
		return ok(HttpCode.OK, data);
	}

	/** 设置响应代码 */
	public static Object ok(HttpCode code, Object data) {
		Map<String, Object> map = Maps.newLinkedHashMap();
		Map<String, Object> result = Maps.newLinkedHashMap();
		for (Iterator<String> iterator = map.keySet().iterator(); iterator.hasNext();) {
			String key = iterator.next();
			if (!key.startsWith("org.springframework.validation.BindingResult") && !key.equals("void")) {
				result.put(key, map.get(key));
			}
		}
		if (data != null) {
			if (data instanceof PageInfo) {
				PageInfo<?> page = (PageInfo<?>) data;
				result.put("rows", page.getList());
				result.put("total", page.getTotal());
			} else {
				result.put("data", data);
			}
		}
		result.put("code", code.value());
		result.put("msg", code.msg());
		result.put("timestamp", System.currentTimeMillis());
		return result;
	}
}

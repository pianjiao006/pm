package com.pimee.common.utils;

import java.util.Random;
import java.util.UUID;
/**
 * 有待改进，不要用StringBuffer，用更高效的char数组
 * @author Bruce Shaw
 * 2020年4月13日 下午3:13:20
 */
public class RandomUtil {
	private static char[] chars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
			'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B',
			'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
			'X', 'Y', 'Z' };

	private static char[] charsNoNumber = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
			'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
			'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

	public static String getUUID36Lowercase() {
		return UUID.randomUUID().toString();
	}

	public static String getUUID32Lowercase() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	public static String getUUID36Uppercase() {
		return UUID.randomUUID().toString().toUpperCase();
	}

	public static String getUUID32Uppercase() {
		return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
	}

	/**
	 * 
	 * @param digi
	 *            正整数，想要获取多少位的数字字符
	 * @return
	 */
	public static String getRandomStr(int digi) {
		if (digi <= 0) {
			throw new RuntimeException("getRandomStr入参错误 digi:" + digi);
		}
		StringBuffer buf = new StringBuffer();
		Random r = new Random();
		for (int i = 0; i < digi; i++) {
			buf.append(r.nextInt(10));
		}
		return buf.toString();
	}

	/**
	 * 获得指定位数的字符串
	 * 
	 * @param digi
	 * @return
	 * @author xiaoxb
	 */
	public static String getRandomChar(int digi) {
		if (digi <= 0) {
			throw new RuntimeException("getRandomChar入参错误 digi:" + digi);
		}
		StringBuffer buf = new StringBuffer();
		Random r = new Random();
		int index = 0;
		for (int i = 0; i < digi; i++) {
			index = r.nextInt(chars.length);
			buf.append(chars[index]);
		}
		return buf.toString();
	}

	/**
	 * 获得指定位数的字符串(不包括数字)
	 * 
	 * @param digi
	 * @return
	 * @author xiaoxb
	 */
	public static String getRandomCharNoNumber(int digi) {
		if (digi <= 0) {
			throw new RuntimeException("getRandomCharNoNumber入参错误 digi:" + digi);
		}
		StringBuffer buf = new StringBuffer();
		Random r = new Random();
		int index = 0;
		for (int i = 0; i < digi; i++) {
			index = r.nextInt(charsNoNumber.length);
			buf.append(charsNoNumber[index]);
		}
		return buf.toString();
	}
}

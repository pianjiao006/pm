package com.pimee.common.exception;

/**
 * 
 * @ClassName: IllegalParameterException
 * @Description: TODO<尽量简短描述其作用>
 * @author Bruce Shaw 2020年1月6日 08:46:11
 *
 */
public class IllegalParameterException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int code;
	private String msg;
	private Object data;

	public IllegalParameterException(String msg) {
		this.msg = msg;
	}

	public IllegalParameterException(int code, String msg) {
		super(msg);
		this.code = code;
		this.msg = msg;
		this.data = null;
	}

	public IllegalParameterException(int code, String msg, Object data) {
		super(msg);
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}

package com.pimee.common.exception;

import com.pimee.common.core.support.HttpCode;

/**
 * 验证码失效异常类
 * 
 * @author Bruce Shaw 2020年4月13日 下午5:08:54
 */
public class CaptchaExpireException extends BusinessException {
	private static final long serialVersionUID = 1L;

	public CaptchaExpireException(String msg) {
		super(msg);
		this.setCode(HttpCode.INTERNAL_SERVER_ERROR.value());
	}
}

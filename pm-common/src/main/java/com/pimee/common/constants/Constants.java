package com.pimee.common.constants;

import io.jsonwebtoken.Claims;

/**
 * 通用常量信息
 * 
 * @author ruoyi
 */
public class Constants {
	/**
	 * UTF-8 字符集
	 */
	public static final String UTF8 = "UTF-8";

	/**
	 * 通用成功标识
	 */
	public static final String SUCCESS = "0";

	/**
	 * 通用失败标识
	 */
	public static final String FAIL = "1";

	/**
	 * 登录成功
	 */
	public static final String LOGIN_SUCCESS = "Success";

	/**
	 * 注销
	 */
	public static final String LOGOUT = "Logout";

	/**
	 * 登录失败
	 */
	public static final String LOGIN_FAIL = "Error";

	/**
	 * 验证码 redis key
	 */
	public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

	/**
	 * 登录用户 redis key
	 */
	public static final String LOGIN_TOKEN_KEY = "login_tokens:";

	/**
	 * 验证码有效期（分钟）
	 */
	public static final Integer CAPTCHA_EXPIRATION = 2;

	/**
	 * 令牌
	 */
	public static final String TOKEN = "token";

	/**
	 * 令牌前缀
	 */
	public static final String TOKEN_PREFIX = "Bearer ";

	/**
	 * 令牌前缀
	 */
	public static final String LOGIN_USER_KEY = "login_user_key";

	/**
	 * 用户ID
	 */
	public static final String JWT_USERID = "userid";

	/**
	 * 用户名称
	 */
	public static final String JWT_USERNAME = Claims.SUBJECT;

	/**
	 * 用户头像
	 */
	public static final String JWT_AVATAR = "avatar";

	/**
	 * 创建时间
	 */
	public static final String JWT_CREATED = "created";

	/**
	 * 用户权限
	 */
	public static final String JWT_AUTHORITIES = "authorities";

	/**
	 * 资源映射路径 前缀
	 */
	public static final String RESOURCE_PREFIX = "/profile";

	/**
	 * 用户状态
	 * 
	 * @author Bruce Shaw 2020年4月13日 下午5:50:03
	 */
	public enum UserStatus {
		OK("0", "正常"), DISABLE("1", "停用"), DELETED("2", "删除");

		private final String code;
		private final String info;

		UserStatus(String code, String info) {
			this.code = code;
			this.info = info;
		}

		public String getCode() {
			return code;
		}

		public String getInfo() {
			return info;
		}
	}

	public class CacheKey {
		public static final String SHIRO_SESSION_KEY = "shiro:session:key";

		public static final String SYS_CONFIG = "sys:config";

		/***
		 * session key
		 */
		public static final String USER_SESSION_KEY = "admin:user:session:key";
	}

	/**
	 * 数据源类型
	 * 
	 * @description
	 * @author Bruce Shaw 2020年2月9日 下午3:54:22
	 */
	public enum DataSourceType {
		/**
		 * 主库
		 */
		MASTER,
		/**
		 * 从库
		 */
		SLAVE
	}

	/**
	 * 字典管理 cache key
	 */
	public static final String SYS_DICT_KEY = "sys_dict:";

	public enum ScheduleStatus {
		/**
		 * 定时作业状态
		 */
		NORMAL("0", "正常"), PAUSE("1", "停止");

		private final String code;
		private final String msg;

		private ScheduleStatus(String code, String msg) {
			this.code = code;
			this.msg = msg;
		}

		public String getCode() {
			return code;
		}

		public String getMsg() {
			return msg;
		}
	}

	public enum TaskCode {
		TASK_EXISTS, NO_TASK_EXISTS, TASK_ALREADY_STARTED, UNKNOWN, CONFIG_ERROR, TASK_NODE_NOT_AVAILABLE
	}

	public static class File {
		/**
		 * 资源映射路径 前缀
		 */
		public static final String RESOURCE_PREFIX = "/profile";
	}

	/**
	 * 任务调度
	 *
	 * @author Bruce Shaw 2020年2月5日 下午9:15:41
	 */
	public static class Schedule {

		public static final String TASK_CLASS_NAME = "TASK_CLASS_NAME";

		/**
		 * 执行目标key
		 */
		public static final String TASK_PROPERTIES = "TASK_PROPERTIES";

		/**
		 * 默认
		 */
		public static final String MISFIRE_DEFAULT = "0";

		/**
		 * 立即触发执行
		 */
		public static final String MISFIRE_IGNORE_MISFIRES = "1";

		/**
		 * 触发�?次执�?
		 */
		public static final String MISFIRE_FIRE_AND_PROCEED = "2";

		/**
		 * 不触发立即执执行
		 */
		public static final String MISFIRE_DO_NOTHING = "3";
	}

	/**
	 * @author Bruce Shaw 2020年2月5日 下午9:24:59
	 */
	public enum JobStatus {
		SUC("0", "成功"), FAIL("1", "失败");

		private final String code;
		private final String msg;

		private JobStatus(String code, String msg) {
			this.code = code;
			this.msg = msg;
		}

		public String getCode() {
			return code;
		}

		public String getMsg() {
			return msg;
		}

	}
}

import request from '@/utils/request'

//上传文件
export function uploadFile(data) {
  return request({
    url: '/system/file/upload',
    method: 'post',
    data: data
  })
}

// 校验文件是否存在配置
export function checkExist(fileName) {
  return request({
    url: '/system/file/checkExist/' + fileName,
    method: 'get'
  })
}

// 删除文件配置
export function removeFile(fileName) {
  return request({
    url: '/system/file/remove/' + fileName,
    method: 'delete'
  })
}

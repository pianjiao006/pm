/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : localhost:3306
 Source Schema         : pm-vue

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 29/05/2021 22:06:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `blob_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('UbScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('UbScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('UbScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `entry_id` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('UbScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.pimee.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001A636F6D2E70696D65652E6A6F622E6D6F64656C2E5379734A6F62000000000000000102000D4C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000672656D61726B71007E00094C000673746174757371007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000A78707400013174000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001797EAD44387874000E302F3130202A202A202A202A203F740011706D5461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC89740001337400007400013174000561646D696E7371007E000F770800000179B29BE843787800);
INSERT INTO `qrtz_job_details` VALUES ('UbScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.pimee.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001A636F6D2E70696D65652E6A6F622E6D6F64656C2E5379734A6F62000000000000000102000D4C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000672656D61726B71007E00094C000673746174757371007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000A78707400013174000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001797EAD44387874000E302F3135202A202A202A202A203F740015706D5461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC89740001337400007400013174000561646D696E7371007E000F770800000179B29C04A8787800);
INSERT INTO `qrtz_job_details` VALUES ('UbScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.pimee.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001A636F6D2E70696D65652E6A6F622E6D6F64656C2E5379734A6F62000000000000000102000D4C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000672656D61726B71007E00094C000673746174757371007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000A78707400013174000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001797EAD44387874000E302F3230202A202A202A202A203F740036706D5461736B2E6D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC89740001337400007400013074000561646D696E7371007E000F770800000179B29FBEED787800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('UbScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('UbScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('UbScheduler', 'DESKTOP-OTJDL211622270166468', 1622297212923, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `int_prop_1` int(11) NULL DEFAULT NULL,
  `int_prop_2` int(11) NULL DEFAULT NULL,
  `long_prop_1` bigint(20) NULL DEFAULT NULL,
  `long_prop_2` bigint(20) NULL DEFAULT NULL,
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL,
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL,
  `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `next_fire_time` bigint(13) NULL DEFAULT NULL,
  `prev_fire_time` bigint(13) NULL DEFAULT NULL,
  `priority` int(11) NULL DEFAULT NULL,
  `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) NULL DEFAULT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `misfire_instr` smallint(2) NULL DEFAULT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('UbScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1622199240000, -1, 5, 'PAUSED', 'CRON', 1622199232000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('UbScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1622199240000, -1, 5, 'PAUSED', 'CRON', 1622199239000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('UbScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1622218920000, 1622218900000, 5, 'PAUSED', 'CRON', 1622199484000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2021-05-18 16:52:35', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2021-05-18 16:52:35', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2021-05-18 16:52:35', '', NULL, '深色主题theme-dark，浅色主题theme-light');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `tenant_id` bigint(20) NOT NULL DEFAULT 1 COMMENT '隶属租户',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 112 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 11:30:03', 2);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 11:30:03', 2);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 11:30:03', 2);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 11:30:03', 2);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 11:30:03', 2);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 11:30:03', 2);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 11:30:03', 2);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 6, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 11:30:03', 2);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 11:30:03', 2);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 11:30:03', 2);
INSERT INTO `sys_dept` VALUES (110, 0, '0', '码里物里科技', 0, NULL, NULL, NULL, '0', '0', 'admin', NULL, 'admin', '2021-05-28 12:08:41', 3);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-21 23:11:31', '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2021-05-18 16:52:35', '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'pmTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 18:53:53', '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'pmTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 18:54:00', '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 18:58:04', '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 341 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------
INSERT INTO `sys_job_log` VALUES (1, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：3毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (2, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (3, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (4, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (5, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (6, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (7, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：177毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (8, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (9, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (10, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (11, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (12, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (13, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (14, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (15, '系统默认（多参）', 'DEFAULT', 'pmTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '1', 'java.lang.NoSuchMethodException: com.pimee.job.task.PmTask.ryMultipleParams(java.lang.String, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Integer)\r\n	at java.lang.Class.getDeclaredMethod(Class.java:2130)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:54)\r\n	at com.pimee.job.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.pimee.job.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:18)\r\n	at com.pimee.job.AbstractQuartzJob.execute(AbstractQuartzJob.java:41)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)', NULL);
INSERT INTO `sys_job_log` VALUES (16, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：2826毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (17, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：2毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (18, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：199毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (19, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：8毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (20, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (21, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (22, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (23, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (24, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (25, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (26, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (27, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (28, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (29, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (30, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (31, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (32, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (33, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (34, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (35, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (36, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (37, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (38, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (39, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (40, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (41, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (42, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (43, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (44, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (45, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (46, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (47, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (48, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (49, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (50, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (51, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (52, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (53, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (54, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (55, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (56, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (57, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (58, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (59, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (60, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (61, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (62, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (63, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (64, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (65, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (66, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (67, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (68, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (69, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (70, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (71, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (72, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：182毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (73, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (74, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (75, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (76, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (77, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (78, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (79, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (80, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (81, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (82, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (83, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (84, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (85, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (86, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (87, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (88, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (89, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (90, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (91, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (92, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (93, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (94, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：190毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (95, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (96, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (97, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (98, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (99, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：188毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (100, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (101, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：2毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (102, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (103, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (104, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (105, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (106, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (107, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (108, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (109, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (110, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (111, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (112, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (113, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (114, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (115, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (116, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (117, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (118, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (119, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (120, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (121, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (122, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (123, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (124, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (125, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (126, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (127, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (128, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (129, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (130, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (131, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (132, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (133, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (134, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (135, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (136, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (137, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (138, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (139, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (140, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (141, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (142, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (143, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (144, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (145, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (146, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (147, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (148, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (149, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (150, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (151, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (152, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (153, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (154, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (155, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (156, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (157, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (158, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (159, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (160, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (161, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (162, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (163, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (164, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (165, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (166, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (167, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (168, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (169, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (170, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (171, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (172, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (173, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (174, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (175, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：8毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (176, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (177, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (178, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (179, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (180, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (181, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (182, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (183, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (184, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (185, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (186, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (187, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (188, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (189, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (190, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (191, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (192, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (193, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (194, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (195, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (196, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (197, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (198, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (199, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (200, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (201, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (202, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (203, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：34毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (204, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (205, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (206, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (207, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (208, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (209, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (210, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (211, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (212, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (213, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (214, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (215, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (216, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (217, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (218, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (219, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (220, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (221, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (222, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (223, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (224, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (225, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (226, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (227, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (228, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (229, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (230, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (231, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (232, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (233, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (234, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (235, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (236, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (237, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (238, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (239, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (240, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (241, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (242, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (243, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (244, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (245, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (246, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (247, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (248, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (249, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (250, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (251, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (252, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (253, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (254, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (255, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (256, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (257, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (258, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (259, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (260, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (261, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (262, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (263, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (264, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (265, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (266, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (267, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (268, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (269, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (270, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (271, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (272, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (273, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (274, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：10毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (275, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (276, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (277, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (278, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (279, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (280, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (281, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (282, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (283, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (284, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (285, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：8毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (286, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (287, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (288, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (289, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (290, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (291, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (292, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (293, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (294, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (295, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (296, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (297, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (298, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (299, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：11毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (300, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (301, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (302, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (303, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (304, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (305, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (306, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (307, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：9毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (308, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (309, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (310, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (311, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (312, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (313, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (314, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (315, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (316, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (317, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (318, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (319, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (320, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (321, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (322, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (323, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (324, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (325, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (326, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (327, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (328, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (329, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (330, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (331, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (332, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (333, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：9毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (334, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (335, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (336, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (337, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (338, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (339, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：0毫秒', '0', '', NULL);
INSERT INTO `sys_job_log` VALUES (340, '系统默认（多参）', 'DEFAULT', 'pmTask.multipleParams(\'ry\', true, 2000L, 316.50D, 100)', '系统默认（多参） 总共耗时：1毫秒', '0', '', NULL);

-- ----------------------------
-- Table structure for sys_login_in_for
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_in_for`;
CREATE TABLE `sys_login_in_for`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_login_in_for
-- ----------------------------
INSERT INTO `sys_login_in_for` VALUES (1, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', NULL);
INSERT INTO `sys_login_in_for` VALUES (2, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', NULL);
INSERT INTO `sys_login_in_for` VALUES (3, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', NULL);
INSERT INTO `sys_login_in_for` VALUES (4, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', NULL);
INSERT INTO `sys_login_in_for` VALUES (5, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', NULL);
INSERT INTO `sys_login_in_for` VALUES (6, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', NULL);
INSERT INTO `sys_login_in_for` VALUES (7, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', NULL);
INSERT INTO `sys_login_in_for` VALUES (8, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', NULL);
INSERT INTO `sys_login_in_for` VALUES (9, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', NULL);
INSERT INTO `sys_login_in_for` VALUES (10, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', NULL);
INSERT INTO `sys_login_in_for` VALUES (11, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-05-28 22:44:26');
INSERT INTO `sys_login_in_for` VALUES (12, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-05-28 22:44:32');
INSERT INTO `sys_login_in_for` VALUES (13, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-28 22:44:41');
INSERT INTO `sys_login_in_for` VALUES (14, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-05-28 22:51:50');
INSERT INTO `sys_login_in_for` VALUES (15, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-05-28 22:51:54');
INSERT INTO `sys_login_in_for` VALUES (16, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-28 22:51:59');
INSERT INTO `sys_login_in_for` VALUES (17, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-28 23:52:53');
INSERT INTO `sys_login_in_for` VALUES (18, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-05-29 00:13:10');
INSERT INTO `sys_login_in_for` VALUES (19, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-29 00:21:12');
INSERT INTO `sys_login_in_for` VALUES (20, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-05-29 00:21:56');
INSERT INTO `sys_login_in_for` VALUES (21, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-29 00:22:00');
INSERT INTO `sys_login_in_for` VALUES (22, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-05-29 00:22:07');
INSERT INTO `sys_login_in_for` VALUES (23, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-29 08:52:47');
INSERT INTO `sys_login_in_for` VALUES (24, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-05-29 14:10:04');
INSERT INTO `sys_login_in_for` VALUES (25, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-29 14:10:13');
INSERT INTO `sys_login_in_for` VALUES (26, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-05-29 14:41:05');
INSERT INTO `sys_login_in_for` VALUES (27, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-29 14:41:13');
INSERT INTO `sys_login_in_for` VALUES (28, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-05-29 14:43:00');
INSERT INTO `sys_login_in_for` VALUES (29, 'test', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-29 14:43:10');
INSERT INTO `sys_login_in_for` VALUES (30, 'test', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-05-29 14:43:19');
INSERT INTO `sys_login_in_for` VALUES (31, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-29 15:55:34');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1065 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, 1, 0, 'M', '0', '0', '', 'system', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-20 18:03:46', '系统管理目录', NULL);
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-20 18:03:50', '系统监控目录', NULL);
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2021-05-18 16:52:35', '', NULL, '系统工具目录', NULL);
INSERT INTO `sys_menu` VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', NULL, 1, 0, 'C', '1', '1', '', 'guide', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-29 00:08:34', '若依官网地址', NULL);
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2021-05-18 16:52:35', '', NULL, '用户管理菜单', NULL);
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2021-05-18 16:52:35', '', NULL, '角色管理菜单', NULL);
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2021-05-18 16:52:35', '', NULL, '菜单管理菜单', NULL);
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2021-05-18 16:52:35', '', NULL, '部门管理菜单', NULL);
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2021-05-18 16:52:35', '', NULL, '岗位管理菜单', NULL);
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2021-05-18 16:52:35', '', NULL, '字典管理菜单', NULL);
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2021-05-18 16:52:35', '', NULL, '参数设置菜单', NULL);
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2021-05-18 16:52:35', '', NULL, '通知公告菜单', NULL);
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2021-05-18 16:52:35', '', NULL, '日志管理菜单', NULL);
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2021-05-18 16:52:35', '', NULL, '在线用户菜单', NULL);
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2021-05-18 16:52:35', '', NULL, '定时任务菜单', NULL);
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2021-05-18 16:52:35', '', NULL, '数据监控菜单', NULL);
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2021-05-18 16:52:35', '', NULL, '服务监控菜单', NULL);
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2021-05-18 16:52:35', '', NULL, '缓存监控菜单', NULL);
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2021-05-18 16:52:35', '', NULL, '表单构建菜单', NULL);
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', 1, 0, 'C', '1', '0', 'tool:gen:list', 'code', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-29 00:08:51', '代码生成菜单', NULL);
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2021-05-18 16:52:35', '', NULL, '系统接口菜单', NULL);
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 10, 'operlog', 'monitor/operlog/index', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 09:50:11', '操作日志菜单', NULL);
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2021-05-18 16:52:35', '', NULL, '登录日志菜单', NULL);
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1040, '操作查询', 500, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1041, '操作删除', 500, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 7, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2021-05-18 16:52:35', '', NULL, '', NULL);
INSERT INTO `sys_menu` VALUES (1061, '租户管理', 1, 4, 'tenant', 'system/tenant/index', 1, 0, 'C', '0', '0', 'system:tenant:list', 'people', 'admin', '2021-05-26 12:02:52', 'admin', '2021-05-28 10:15:44', '', NULL);
INSERT INTO `sys_menu` VALUES (1062, '租户添加', 1061, 1, '', NULL, 1, 0, 'F', '0', '0', 'system:tenant:add', '#', 'admin', '2021-05-26 12:04:45', 'admin', '2021-05-26 12:05:19', '', NULL);
INSERT INTO `sys_menu` VALUES (1063, '更新', 1061, 2, '', NULL, 1, 0, 'F', '0', '0', 'system:tenant:update', '#', 'admin', '2021-05-26 12:05:40', 'admin', '2021-05-26 12:06:03', '', NULL);
INSERT INTO `sys_menu` VALUES (1064, '租户删除', 1061, 3, '', NULL, 1, 0, 'F', '0', '0', 'system:tenant:remove', '#', 'admin', '2021-05-26 12:06:46', '', '2021-05-26 12:06:46', '', NULL);

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', '<p>3423434234sadfasdf沙发大发沙发的沙发</p>', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 10:01:40', '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', '<p>57u05oqk5YaF5a65234@！#@￥哈哈哈</p>', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-22 00:02:57', '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (1, '登录日志', 9, 'com.pimee.web.controller.monitor.SysLogininforController.clean()', 'DELETE', 1, 'admin', '', '/monitor/logininfor/clean', '127.0.0.1', '内网IP', '{}', '{\"code\":200,\"msg\":\"操作成功\",\"timestamp\":1622168101622}', 0, '', '2021-05-28 10:15:02');
INSERT INTO `sys_oper_log` VALUES (2, '新增租户', 1, 'com.pimee.web.controller.sys.SysTenantController.add()', 'POST', 1, 'admin', '', '/system/tenant', '127.0.0.1', '内网IP', '{\"principal\":\"啵啵\",\"createBy\":\"admin\",\"tenantName\":\"码里物理科技\",\"createTime\":1622170597847,\"updateBy\":\"admin\",\"tenantId\":2,\"updateTime\":1622170597847,\"tenantCode\":\"maliwuli\",\"enabled\":0}', '{\"code\":200,\"msg\":\"操作成功\",\"timestamp\":1622170597872}', 0, '', '2021-05-28 10:56:38');
INSERT INTO `sys_oper_log` VALUES (3, '删除租户', 3, 'com.pimee.web.controller.sys.SysTenantController.remove()', 'DELETE', 1, 'admin', '', '/system/tenant/2', '127.0.0.1', '内网IP', '{tenantIds=2}', '{\"code\":200,\"msg\":\"操作成功\",\"timestamp\":1622173597026}', 0, '', '2021-05-28 11:46:37');
INSERT INTO `sys_oper_log` VALUES (4, '新增租户', 1, 'com.pimee.web.controller.sys.SysTenantController.add()', 'POST', 1, 'admin', '', '/system/tenant', '127.0.0.1', '内网IP', '{\"principal\":\"啵啵\",\"createBy\":\"admin\",\"address\":\"广州天河\",\"tenantName\":\"码里物理科技\",\"createTime\":1622173648554,\"phone\":\"13800138000\",\"updateBy\":\"admin\",\"tenantId\":3,\"updateTime\":1622173648554,\"tenantCode\":\"MaliWuli\",\"enabled\":0}', '{\"code\":200,\"msg\":\"操作成功\",\"timestamp\":1622173648596}', 0, '', '2021-05-28 11:47:29');
INSERT INTO `sys_oper_log` VALUES (5, '删除租户', 3, 'com.pimee.web.controller.sys.SysTenantController.remove()', 'DELETE', 1, 'admin', '', '/system/tenant/1', '127.0.0.1', '内网IP', '{tenantIds=1}', 'null', 1, '', '2021-05-28 11:59:58');
INSERT INTO `sys_oper_log` VALUES (6, '删除租户', 3, 'com.pimee.web.controller.sys.SysTenantController.remove()', 'DELETE', 1, 'admin', '', '/system/tenant/1', '127.0.0.1', '内网IP', '{tenantIds=1}', 'null', 1, '', '2021-05-28 12:00:12');
INSERT INTO `sys_oper_log` VALUES (7, '删除租户', 3, 'com.pimee.web.controller.sys.SysTenantController.remove()', 'DELETE', 1, 'admin', '', '/system/tenant/1', '127.0.0.1', '内网IP', '{tenantIds=1}', 'null', 1, '', '2021-05-28 12:01:10');
INSERT INTO `sys_oper_log` VALUES (8, '删除租户', 3, 'com.pimee.web.controller.sys.SysTenantController.remove()', 'DELETE', 1, 'admin', '', '/system/tenant/1', '127.0.0.1', '内网IP', '{tenantIds=1}', 'null', 1, '当前租户关联部门, 不允许删除', '2021-05-28 12:02:34');
INSERT INTO `sys_oper_log` VALUES (9, '删除租户', 3, 'com.pimee.web.controller.sys.SysTenantController.remove()', 'DELETE', 1, 'admin', '', '/system/tenant/1', '127.0.0.1', '内网IP', '{tenantIds=1}', 'null', 1, '当前租户关联部门, 不允许删除', '2021-05-28 12:02:39');
INSERT INTO `sys_oper_log` VALUES (10, '删除租户', 3, 'com.pimee.web.controller.sys.SysTenantController.remove()', 'DELETE', 1, 'admin', '', '/system/tenant/1', '127.0.0.1', '内网IP', '{tenantIds=1}', '{\"code\":200,\"msg\":\"操作成功\",\"timestamp\":1622174662501}', 0, '', '2021-05-28 12:04:23');
INSERT INTO `sys_oper_log` VALUES (11, '删除租户', 3, 'com.pimee.web.controller.sys.SysTenantController.remove()', 'DELETE', 1, 'admin', '', '/system/tenant/3', '127.0.0.1', '内网IP', '{tenantIds=3}', 'null', 1, '当前租户关联部门, 不允许删除', '2021-05-28 12:04:30');
INSERT INTO `sys_oper_log` VALUES (12, '修改租户', 2, 'com.pimee.web.controller.sys.SysTenantController.edit()', 'PUT', 1, 'admin', '', '/system/tenant', '127.0.0.1', '内网IP', '{\"principal\":\"啵啵\",\"createBy\":\"admin\",\"address\":\"广州天河\",\"tenantName\":\"码里物里科技\",\"createTime\":1622173649000,\"phone\":\"13800138000\",\"updateBy\":\"admin\",\"tenantId\":3,\"updateTime\":1622174695606,\"tenantCode\":\"MaliWuli\",\"enabled\":0}', '{\"code\":200,\"msg\":\"操作成功\",\"timestamp\":1622174695616}', 0, '', '2021-05-28 12:04:56');
INSERT INTO `sys_oper_log` VALUES (13, '在线用户强制退出', 7, 'com.pimee.web.controller.monitor.SysUserOnlineController.forceLogout()', 'DELETE', 1, '', '', '/monitor/online/fd74793f-1667-44c4-ae83-d5562df1acf6', '127.0.0.1', '内网IP', '{tokenId=fd74793f-1667-44c4-ae83-d5562df1acf6}', '{\"code\":200,\"msg\":\"操作成功\",\"timestamp\":1622188172051}', 0, '', '2021-05-28 15:49:32');
INSERT INTO `sys_oper_log` VALUES (14, '', 0, 'com.pimee.job.web.SysJobController.run()', 'PUT', 0, 'admin', '', '/monitor/job/run', '127.0.0.1', '内网IP', '', '{\"code\":200,\"msg\":\"操作成功\",\"timestamp\":1622199401777}', 0, '', '2021-05-28 18:56:42');
INSERT INTO `sys_oper_log` VALUES (15, '', 0, 'com.pimee.job.web.SysJobController.edit()', 'PUT', 0, 'admin', '', '/monitor/job', '127.0.0.1', '内网IP', '', '{\"code\":200,\"msg\":\"操作成功\",\"timestamp\":1622199484171}', 0, '', '2021-05-28 18:58:04');
INSERT INTO `sys_oper_log` VALUES (16, '', 0, 'com.pimee.job.web.SysJobController.changeStatus()', 'PUT', 0, 'admin', '', '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '', '{\"code\":200,\"msg\":\"操作成功\",\"timestamp\":1622218910973}', 0, '', '2021-05-29 00:21:51');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 22:33:15', '');
INSERT INTO `sys_post` VALUES (2, 'manager', '项目经理', 2, '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-21 18:40:29', '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2021-05-18 16:52:35', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2021-05-18 16:52:35', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-29 00:21:21', '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2021-05-18 16:52:35', '', '2021-05-18 18:42:05', '普通角色');
INSERT INTO `sys_role` VALUES (102, '测试', 'test', 1, '3', 1, 1, '0', '0', 'admin', '2021-05-18 19:10:33', 'admin', '2021-05-20 17:47:14', NULL);

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100, '2021-05-18 18:42:04');
INSERT INTO `sys_role_dept` VALUES (2, 101, '2021-05-18 18:42:04');
INSERT INTO `sys_role_dept` VALUES (2, 104, '2021-05-18 18:42:04');
INSERT INTO `sys_role_dept` VALUES (2, 105, '2021-05-18 18:42:04');
INSERT INTO `sys_role_dept` VALUES (2, 106, '2021-05-18 18:42:04');
INSERT INTO `sys_role_dept` VALUES (100, 100, '2021-05-18 18:49:51');
INSERT INTO `sys_role_dept` VALUES (100, 101, '2021-05-18 18:49:51');
INSERT INTO `sys_role_dept` VALUES (100, 102, '2021-05-18 18:49:51');
INSERT INTO `sys_role_dept` VALUES (100, 103, '2021-05-18 18:49:51');
INSERT INTO `sys_role_dept` VALUES (100, 104, '2021-05-18 18:49:51');
INSERT INTO `sys_role_dept` VALUES (100, 105, '2021-05-18 18:49:51');
INSERT INTO `sys_role_dept` VALUES (100, 106, '2021-05-18 18:49:51');
INSERT INTO `sys_role_dept` VALUES (100, 107, '2021-05-18 18:49:51');
INSERT INTO `sys_role_dept` VALUES (100, 108, '2021-05-18 18:49:51');
INSERT INTO `sys_role_dept` VALUES (100, 109, '2021-05-18 18:49:51');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 2, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 3, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 100, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 101, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 102, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 103, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 104, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 105, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 106, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 107, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 108, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 109, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 110, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 111, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 112, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 113, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 114, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 115, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 116, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 500, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (1, 501, '2021-05-29 00:21:21');
INSERT INTO `sys_role_menu` VALUES (2, 1, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 2, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 3, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 4, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 100, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 101, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 102, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 103, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 104, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 105, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 106, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 107, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 108, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 109, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 110, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 111, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 112, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 113, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 114, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 115, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 116, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 500, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 501, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1000, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1001, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1002, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1003, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1004, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1005, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1006, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1007, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1008, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1009, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1010, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1011, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1012, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1013, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1014, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1015, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1016, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1017, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1018, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1019, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1020, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1021, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1022, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1023, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1024, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1025, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1026, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1027, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1028, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1029, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1030, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1031, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1032, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1033, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1034, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1035, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1036, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1037, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1038, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1039, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1040, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1041, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1042, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1043, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1044, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1045, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1046, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1047, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1048, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1049, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1050, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1051, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1052, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1053, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1054, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1055, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1056, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1057, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1058, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1059, NULL);
INSERT INTO `sys_role_menu` VALUES (2, 1060, NULL);
INSERT INTO `sys_role_menu` VALUES (102, 1, '2021-05-18 19:10:46');
INSERT INTO `sys_role_menu` VALUES (102, 101, '2021-05-18 19:10:46');
INSERT INTO `sys_role_menu` VALUES (102, 102, '2021-05-18 19:10:46');
INSERT INTO `sys_role_menu` VALUES (102, 103, '2021-05-18 19:10:46');
INSERT INTO `sys_role_menu` VALUES (102, 104, '2021-05-18 19:10:46');
INSERT INTO `sys_role_menu` VALUES (102, 105, '2021-05-18 19:10:46');
INSERT INTO `sys_role_menu` VALUES (102, 106, '2021-05-18 19:10:46');
INSERT INTO `sys_role_menu` VALUES (102, 107, '2021-05-18 19:10:46');
INSERT INTO `sys_role_menu` VALUES (102, 108, '2021-05-18 19:10:46');
INSERT INTO `sys_role_menu` VALUES (102, 500, '2021-05-18 19:10:46');
INSERT INTO `sys_role_menu` VALUES (102, 501, '2021-05-18 19:10:46');

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant`  (
  `tenant_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '租户的ID',
  `tenant_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '租户名称',
  `tenant_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户编码',
  `principal` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `address` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `enabled` int(3) NULL DEFAULT 1 COMMENT '启用状态:0|未启用，1|启用',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `create_by` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `update_time` datetime(0) NOT NULL,
  `update_by` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`tenant_id`) USING BTREE,
  UNIQUE INDEX `idx_tenant_code`(`tenant_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '租户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_tenant
-- ----------------------------
INSERT INTO `sys_tenant` VALUES (3, '码里物里科技', 'MaliWuli', '啵啵', '13800138000', '广州天河1', 1, NULL, '2021-05-28 11:47:29', 'admin', '2021-05-29 00:21:34', 'admin');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$yPWMzWIoCM0VdRw1umItWer6Yin.fVn1u3KN4JLs8frUSFlP4Uak2', '0', '0', '127.0.0.1', '2021-05-18 16:52:35', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-26 11:19:21', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2021-05-18 16:52:35', 'admin', '2021-05-18 16:52:35', 'admin', '2021-05-28 10:01:32', '测试员');
INSERT INTO `sys_user` VALUES (3, 102, 'test', '测试员', '01', '', '13340004100', '0', '', '$2a$10$tNcRy1oC/CzXnU6gk8O7keJAUt07sp3PdOZ8fgf0mmnZDd5T1fGWa', '0', '0', '', NULL, 'admin', '2021-05-29 14:42:40', '', '2021-05-29 14:42:40', NULL);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1, '2021-05-18 17:52:31');
INSERT INTO `sys_user_post` VALUES (2, 2, '2021-05-28 10:01:32');
INSERT INTO `sys_user_post` VALUES (3, 2, '2021-05-29 14:42:40');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1, '2021-05-18 17:52:31');
INSERT INTO `sys_user_role` VALUES (1, 2, '2021-05-18 17:52:31');
INSERT INTO `sys_user_role` VALUES (2, 2, '2021-05-28 10:01:32');

-- ----------------------------
-- Function structure for listChilDept
-- ----------------------------
DROP FUNCTION IF EXISTS `listChilDept`;
delimiter ;;
CREATE FUNCTION `listChilDept`(deptId TEXT(65536))
 RETURNS mediumtext CHARSET utf8mb4
  DETERMINISTIC
BEGIN
DECLARE sTemp TEXT(65536);
DECLARE sTempChd TEXT(65536);

SET sTemp='$';
SET sTempChd = deptId;

WHILE sTempChd IS NOT NULL DO
SET sTemp= CONCAT(sTemp,',',sTempChd);
SELECT GROUP_CONCAT(dept_id) INTO sTempChd FROM sys_dept WHERE FIND_IN_SET(parent_id,sTempChd)>0;
END WHILE;
RETURN sTemp;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;

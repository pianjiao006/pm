package com.pimee.model.vo;

import java.util.List;

import com.pimee.model.SysDept;
import com.pimee.model.SysRole;
import com.pimee.model.SysUser;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SysUserVo extends SysUser {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 角色对象 */
	private List<SysRole> roles;

	private Long[] postIds;

	private SysDept dept;

	/** 角色组 */
	private Long[] roleIds;

	/**
	 * 判断用户id是否管理员
	 * 
	 * @return
	 */
	public boolean isAdmin() {
		return isAdmin(this.getUserId());
	}

	public static boolean isAdmin(Long userId) {
		return userId != null && 1L == userId;
	}
}

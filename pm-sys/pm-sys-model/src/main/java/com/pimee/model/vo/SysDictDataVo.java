package com.pimee.model.vo;

import com.pimee.model.SysDictData;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SysDictDataVo extends SysDictData {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8661463102717171407L;
	private boolean isOk;

}

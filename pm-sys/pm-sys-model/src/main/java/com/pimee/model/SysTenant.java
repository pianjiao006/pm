package com.pimee.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`sys_tenant`")
public class SysTenant implements Serializable {
    /**
     * 租户的ID
     */
    @Id
    @Column(name = "`tenant_id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long tenantId;

    /**
     * 租户名称
     */
    @Column(name = "`tenant_name`")
    private String tenantName;

    /**
     * 租户编码
     */
    @Column(name = "`tenant_code`")
    private String tenantCode;

    /**
     * 负责人
     */
    @Column(name = "`principal`")
    private String principal;

    /**
     * 联系电话
     */
    @Column(name = "`phone`")
    private String phone;

    /**
     * 地址
     */
    @Column(name = "`address`")
    private String address;

    /**
     * 启用状态:0|未启用，1|启用
     */
    @Column(name = "`enabled`")
    private Integer enabled;

    @Column(name = "`remark`")
    private String remark;

    @Column(name = "`create_time`")
    private Date createTime;

    @Column(name = "`create_by`")
    private String createBy;

    @Column(name = "`update_time`")
    private Date updateTime;

    @Column(name = "`update_by`")
    private String updateBy;

    /**
     * 接口标识
     */
    @Column(name = "`app_id`")
    private String appId;

    /**
     * 接口密码
     */
    @Column(name = "`app_secret`")
    private String appSecret;

    /**
     * 购买时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "`buy_time`")
    private Date buyTime;

    /**
     * 过期时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "`expire_time`")
    private Date expireTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取租户的ID
     *
     * @return tenant_id - 租户的ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 设置租户的ID
     *
     * @param tenantId 租户的ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 获取租户名称
     *
     * @return tenant_name - 租户名称
     */
    public String getTenantName() {
        return tenantName;
    }

    /**
     * 设置租户名称
     *
     * @param tenantName 租户名称
     */
    public void setTenantName(String tenantName) {
        this.tenantName = tenantName == null ? null : tenantName.trim();
    }

    /**
     * 获取租户编码
     *
     * @return tenant_code - 租户编码
     */
    public String getTenantCode() {
        return tenantCode;
    }

    /**
     * 设置租户编码
     *
     * @param tenantCode 租户编码
     */
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode == null ? null : tenantCode.trim();
    }

    /**
     * 获取负责人
     *
     * @return principal - 负责人
     */
    public String getPrincipal() {
        return principal;
    }

    /**
     * 设置负责人
     *
     * @param principal 负责人
     */
    public void setPrincipal(String principal) {
        this.principal = principal == null ? null : principal.trim();
    }

    /**
     * 获取联系电话
     *
     * @return phone - 联系电话
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置联系电话
     *
     * @param phone 联系电话
     */
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /**
     * 获取地址
     *
     * @return address - 地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置地址
     *
     * @param address 地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取启用状态:0|未启用，1|启用
     *
     * @return enabled - 启用状态:0|未启用，1|启用
     */
    public Integer getEnabled() {
        return enabled;
    }

    /**
     * 设置启用状态:0|未启用，1|启用
     *
     * @param enabled 启用状态:0|未启用，1|启用
     */
    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return create_by
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * @param createBy
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return update_by
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }

    /**
     * 获取接口标识
     *
     * @return app_id - 接口标识
     */
    public String getAppId() {
        return appId;
    }

    /**
     * 设置接口标识
     *
     * @param appId 接口标识
     */
    public void setAppId(String appId) {
        this.appId = appId == null ? null : appId.trim();
    }

    /**
     * 获取接口密码
     *
     * @return app_secret - 接口密码
     */
    public String getAppSecret() {
        return appSecret;
    }

    /**
     * 设置接口密码
     *
     * @param appSecret 接口密码
     */
    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret == null ? null : appSecret.trim();
    }

    /**
     * 获取购买时间
     *
     * @return buy_time - 购买时间
     */
    public Date getBuyTime() {
        return buyTime;
    }

    /**
     * 设置购买时间
     *
     * @param buyTime 购买时间
     */
    public void setBuyTime(Date buyTime) {
        this.buyTime = buyTime;
    }

    /**
     * 获取过期时间
     *
     * @return expire_time - 过期时间
     */
    public Date getExpireTime() {
        return expireTime;
    }

    /**
     * 设置过期时间
     *
     * @param expireTime 过期时间
     */
    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }
}
package com.pimee.model.vo;

import com.pimee.model.SysTenant;
import lombok.Data;

@Data
public class SysTenantVo extends SysTenant {
    Integer renewTimeType; // 续费时长类型
}

package com.pimee.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`sys_tenant`")
public class SysUnit implements Serializable {
    /**
     * 租户的ID
     */
    @Id
    @Column(name = "`tenant_id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long tenantId;

    /**
     * 租户名称
     */
    @Column(name = "`tenant_name`")
    private String tenantName;

    /**
     * 租户编码
     */
    @Column(name = "`tenant_code`")
    private String tenantCode;

    /**
     * 负责人
     */
    @Column(name = "`principal`")
    private String principal;

    /**
     * 联系电话
     */
    @Column(name = "`phone`")
    private String phone;

    /**
     * 地址
     */
    @Column(name = "`address`")
    private String address;

    /**
     * 启用状态:0|未启用，1|启用
     */
    @Column(name = "`enabled`")
    private Boolean enabled;

    @Column(name = "`remark`")
    private String remark;

    @Column(name = "`create_time`")
    private Date createTime;

    @Column(name = "`create_by`")
    private String createBy;

    @Column(name = "`update_time`")
    private Date updateTime;

    @Column(name = "`update_by`")
    private String updateBy;

    private static final long serialVersionUID = 1L;

    /**
     * 获取租户的ID
     *
     * @return tenant_id - 租户的ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 设置租户的ID
     *
     * @param tenantId 租户的ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 获取租户名称
     *
     * @return tenant_name - 租户名称
     */
    public String getTenantName() {
        return tenantName;
    }

    /**
     * 设置租户名称
     *
     * @param tenantName 租户名称
     */
    public void setTenantName(String tenantName) {
        this.tenantName = tenantName == null ? null : tenantName.trim();
    }

    /**
     * 获取租户编码
     *
     * @return tenant_code - 租户编码
     */
    public String getTenantCode() {
        return tenantCode;
    }

    /**
     * 设置租户编码
     *
     * @param tenantCode 租户编码
     */
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode == null ? null : tenantCode.trim();
    }

    /**
     * 获取负责人
     *
     * @return principal - 负责人
     */
    public String getPrincipal() {
        return principal;
    }

    /**
     * 设置负责人
     *
     * @param principal 负责人
     */
    public void setPrincipal(String principal) {
        this.principal = principal == null ? null : principal.trim();
    }

    /**
     * 获取联系电话
     *
     * @return phone - 联系电话
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置联系电话
     *
     * @param phone 联系电话
     */
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /**
     * 获取地址
     *
     * @return address - 地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置地址
     *
     * @param address 地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取启用状态:0|未启用，1|启用
     *
     * @return enabled - 启用状态:0|未启用，1|启用
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * 设置启用状态:0|未启用，1|启用
     *
     * @param enabled 启用状态:0|未启用，1|启用
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return create_by
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * @param createBy
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return update_by
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}
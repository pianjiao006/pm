package com.pimee.service.sys.impl;

import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.pimee.common.constants.Constants.CacheKey;
import com.pimee.common.core.support.HttpCode;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.service.impl.BaseService;
import com.pimee.common.utils.DateUtil;
import com.pimee.common.utils.RedisUtil;
import com.pimee.common.utils.StringUtils;
import com.pimee.mapper.ext.ExtSysConfigMapper;
import com.pimee.model.SysConfig;
import com.pimee.service.sys.ISysConfigService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unchecked")
@Slf4j
@Service
public class SysConfigService extends BaseService<SysConfig> implements ISysConfigService {

	@Resource
	private ExtSysConfigMapper extSysConfigMapper;
	@SuppressWarnings("rawtypes")
	@Resource
	private RedisUtil redisUtil;

	@PostConstruct()
	public void init() {
		reloadCache();
	}

	/**
	 * 重新加载缓存
	 */
	private void reloadCache() {
		Map<String, Object> resultMap = this.loadAllConfig();
		redisUtil.hash_putMap(CacheKey.SYS_CONFIG, resultMap);
	}

	public Map<String, Object> loadAllConfig() {
		List<SysConfig> list = this.listAll();
		log.info("===> 加载系统配置，init...");
		Map<String, Object> resultMap = Maps.newHashMap();
		for (SysConfig param : list) {
			if (param != null) {
				resultMap.put(param.getConfigKey(), param);
			}
		}

		return resultMap;
	}

	@Override
	public String getConfigValue(String configKey, String defaultValue) {
		Map<String, Object> resultMap = (Map<String, Object>) redisUtil.hash_getField(CacheKey.SYS_CONFIG, configKey);
		if (MapUtils.isEmpty(resultMap)) {
			return defaultValue;
		}
		// 如果缓存不为空 直接返回缓存数据
		String configValue = (String) resultMap.get("configValue");
		if (StringUtils.isNotBlank(configValue)) {
			return configValue;
		}
		// 如果缓存为空 那么直接加载数据库数据
		SysConfig sysConfig = new SysConfig();
		sysConfig.setConfigKey(configKey);
		SysConfig param = mapper.selectOne(sysConfig);
		if (param == null) {
			return defaultValue;
		}
		configValue = param.getConfigValue();
		if (StringUtils.isBlank(configValue)) {
			return defaultValue;
		}
		redisUtil.hash_putField(CacheKey.SYS_CONFIG, configKey, param);
		return configValue;
	}

	@Override
	public String getConfigValue(String configKey) {
		return this.getConfigValue(configKey, "");
	}

	@Override
	public String selectConfigByKeyForce(String configKey) {
		SysConfig config = new SysConfig();
		config.setConfigKey(configKey);
		config = this.selectOne(config);
		return StringUtils.isNotNull(config) ? config.getConfigValue() : "";
	}

	@Override
	public void insertConfig(SysConfig param) {
		Date now = DateUtil.now();
		param.setCreateTime(now);
		param.setUpdateTime(now);
		this.saveNotNull(param);

		reloadCache();
	}

	@Override
	public void updateConfig(SysConfig param) {
		param.setUpdateTime(DateUtil.now());
		this.updateNotNull(param);

		reloadCache();
	}

	@Override
	public PageInfo<SysConfig> pageList(Map<String, Object> params) {
		log.info("===> 配置列表分页查询...");
		startPage(params);// 设置分页
		List<SysConfig> list = extSysConfigMapper.pageList(params);
		return new PageInfo<>(list);
	}

	@Override
	public void deleteByIds(Long[] configIds) {
		if (configIds == null || configIds.length == 0) {
			throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "没有传入任何要删除的标识");
		}
		Example example = new Example(SysConfig.class);
		List<Long> configIdList = Arrays.asList(configIds);
		example.createCriteria().andIn("configId", configIdList);
		this.deleteBy(example);
	}

	@Override
	public void clearCache() {
		// 删除缓存
		redisUtil.hash_delete(CacheKey.SYS_CONFIG);
		// 重新加载缓存
		reloadCache();
	}
}

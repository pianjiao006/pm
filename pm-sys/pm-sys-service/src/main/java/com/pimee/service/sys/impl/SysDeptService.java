package com.pimee.service.sys.impl;

import com.pimee.common.aop.annotation.DataScope;
import com.pimee.common.constants.UserConstants;
import com.pimee.common.core.support.HttpCode;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.service.impl.BaseService;
import com.pimee.common.utils.DateUtil;
import com.pimee.common.utils.SpringContextUtil;
import com.pimee.common.utils.StringUtils;
import com.pimee.mapper.SysRoleMapper;
import com.pimee.mapper.SysUserMapper;
import com.pimee.mapper.ext.ExtSysDeptMapper;
import com.pimee.model.SysDept;
import com.pimee.model.SysRole;
import com.pimee.model.SysUser;
import com.pimee.model.vo.TreeSelect;
import com.pimee.service.sys.ISysDeptService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 代理服务
 *
 * @author Bruce Shaw 2020年10月30日 下午3:00:13
 */
@Service
public class SysDeptService extends BaseService<SysDept> implements ISysDeptService {

    @Resource
    private ExtSysDeptMapper extSysDeptMapper;
    @Resource
    private SysRoleMapper sysRoleMapper;
    @Resource
    private SysUserMapper sysUserMapper;

    @Override
    public List<TreeSelect> buildDeptTreeSelect() {
        // 这样获取实例  有助于拦截器的拦截 如果使用this直接调用 dataScope注解是无法被调用的
        SysDeptService sysDeptService = SpringContextUtil.getBean("sysDeptService");
        List<SysDept> deptList = sysDeptService.listDept(new HashMap<>());
        List<SysDept> deptTrees = buildDeptTree(deptList);
        return deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    /**
     * 构建前端所需要树结构
     *
     * @param deptList 部门列表
     * @return 树结构列表
     */
    @Override
    public List<SysDept> buildDeptTree(List<SysDept> deptList) {
        List<SysDept> returnList = new ArrayList<SysDept>();
        List<Long> tempList = new ArrayList<Long>();
        for (SysDept dept : deptList) {
            tempList.add(dept.getDeptId());
        }
        for (Iterator<SysDept> iterator = deptList.iterator(); iterator.hasNext(); ) {
            SysDept dept = iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dept.getParentId())) {
                recursionFn(deptList, dept);
                returnList.add(dept);
            }
        }
        if (returnList.isEmpty()) {
            returnList = deptList;
        }
        return returnList;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<SysDept> list, SysDept t) {
        // 得到子节点列表
        List<SysDept> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysDept tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<SysDept> getChildList(List<SysDept> list, SysDept t) {
        List<SysDept> tlist = new ArrayList<SysDept>();
        Iterator<SysDept> it = list.iterator();
        while (it.hasNext()) {
            SysDept n = (SysDept) it.next();
            if (StringUtils.isNotNull(n.getParentId()) && n.getParentId().longValue() == t.getDeptId().longValue()) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysDept> list, SysDept t) {
        return getChildList(list, t).size() > 0 ? true : false;
    }


    @Override
    public SysDept selectDeptById(Long deptId) {
        return this.selectByKey(deptId);
    }

    @DataScope(deptAlias = "d", userAlias = "u")
    @Override
    public List<SysDept> listDept(Map<String, Object> params) {
        return extSysDeptMapper.listDept(params);
    }

    @Override
    public void insertDept(SysDept param) {
        if (UserConstants.NOT_UNIQUE.equals(this.checkDeptNameUnique(param))) {
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "修改部门'" + param.getDeptName() + "'失败，部门名称已存在");
        }
        Date now = DateUtil.now();
        param.setCreateTime(now);
        param.setUpdateTime(now);
        this.saveNotNull(param);
    }

    @Override
    public void updateDept(SysDept param) {
        if (param.getParentId().equals(param.getDeptId())) {
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "修改部门'" + param.getDeptName() + "'失败，上级部门不能是自己");
        } else if (StringUtils.equals(UserConstants.DEPT_DISABLE, param.getStatus())
                && extSysDeptMapper.selectNormalChildrenDeptById(param.getDeptId()) > 0) {
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "该部门包含未停用的子部门！");
        }
        Date now = DateUtil.now();
        param.setUpdateTime(now);
        this.updateNotNull(param);

        // 获取部门ID 然后更新子代理的租户归属
        SysDept childParam = new SysDept();
        childParam.setTenantId(param.getTenantId());
        childParam.setUpdateBy(param.getUpdateBy());
        childParam.setDeptId(param.getDeptId());
        extSysDeptMapper.updateChildTenant(childParam);
    }

    @Override
    public void deleteDeptById(Long deptId) {
        Example example = new Example(SysDept.class);
        example.createCriteria().andEqualTo("parentId", deptId);
        int deptCount = this.countByExample(example);
        if (deptCount > 0) {
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "存在下级部门, 不允许删除");
        }

        Example userExample = new Example(SysUser.class);
        userExample.createCriteria().andEqualTo("deptId", deptId);
        int userCount = sysUserMapper.selectCountByExample(userExample);
        if (userCount > 0) {
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "部门存在用户, 不允许删除");
        }

        this.deleteByKey(deptId);
    }

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    @Override
    public List<Integer> selectDeptListByRoleId(Long roleId) {
        SysRole role = sysRoleMapper.selectByPrimaryKey(roleId);
        return extSysDeptMapper.selectDeptListByRoleId(roleId, role.isDeptCheckStrictly());
    }

    /**
     * 校验部门名称是否唯一
     *
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public String checkDeptNameUnique(SysDept dept) {
        Example example = new Example(SysDept.class);
        example.createCriteria().andEqualTo("deptName", dept.getDeptName());
        List<SysDept> deptList = this.selectByExample(example);
        if (CollectionUtils.isNotEmpty(deptList))
            return UserConstants.NOT_UNIQUE;
        return UserConstants.UNIQUE;
    }

}

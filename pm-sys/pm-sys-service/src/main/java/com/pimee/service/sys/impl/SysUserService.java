package com.pimee.service.sys.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.pimee.common.aop.annotation.DataScope;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;
import com.pimee.common.utils.DateUtil;
import com.pimee.common.utils.StringUtils;
import com.pimee.common.utils.bean.BeanUtils;
import com.pimee.common.service.impl.BaseService;
import com.pimee.mapper.SysDeptMapper;
import com.pimee.mapper.SysUserPostMapper;
import com.pimee.mapper.SysUserRoleMapper;
import com.pimee.mapper.ext.ExtSysUserMapper;
import com.pimee.model.SysDept;
import com.pimee.model.SysPost;
import com.pimee.model.SysRole;
import com.pimee.model.SysUser;
import com.pimee.model.SysUserPost;
import com.pimee.model.SysUserRole;
import com.pimee.model.vo.SysUserVo;
import com.pimee.service.sys.ISysUserService;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;

@Slf4j
@Service
public class SysUserService extends BaseService<SysUser> implements ISysUserService {

	@Resource
	private ExtSysUserMapper extSysUserMapper;
	@Resource
	private SysDeptMapper sysDeptMapper;
	@Resource
	private SysUserPostMapper sysUserPostMapper;
	@Resource
	private SysUserRoleMapper sysUserRoleMapper;

	@DataScope(deptAlias = "d", userAlias = "u")
	@Override
	public PageInfo<SysUserVo> pageList(Map<String, Object> params) {
		log.info("===> 用户列表分页查询...");
		startPage(params);// 设置分页
		List<SysUserVo> list = extSysUserMapper.listUser(params);
		return new PageInfo<>(list);
	}

	@Override
	public SysUser selectOneByAccount(String account) {
		SysUser sysUser = new SysUser();
		sysUser.setUserName(account);
		return mapper.selectOne(sysUser);
	}

	@Override
	public void addUser(SysUserVo sysUser) {
		Date now = DateUtil.now();
		sysUser.setCreateTime(now);
		sysUser.setUpdateTime(now);
		sysUser.setDelFlag("0");
		sysUser.setUserType("01"); // 避免生成00
		this.saveNotNull(sysUser);
		// 新增用户与岗位管理
		insertUserPost(sysUser);
	}

	@Override
	public void updateUser(SysUserVo sysUser) {
		Long userId = sysUser.getUserId();
		if (sysUser.getPostIds() != null && sysUser.getPostIds().length > 0) {
			// 删除用户与岗位关联
			Example example = new Example(SysUserPost.class);
			example.createCriteria().andEqualTo("userId", userId);
			sysUserPostMapper.deleteByExample(example);
			// 新增用户与岗位管理
			insertUserPost(sysUser);
		}

		if (sysUser.getRoleIds() != null && sysUser.getRoleIds().length > 0) {
			// 先删除以前的记录
			Example example = new Example(SysUserRole.class);
			example.createCriteria().andEqualTo("userId", userId);
			sysUserRoleMapper.deleteByExample(example);
			// 新增用户与角色管理
			insertUserRole(sysUser);
		}

		sysUser.setUpdateTime(DateUtil.now());
		sysUser.setPassword(null);// 设置为null 不用把密码设置为""空字符串
		this.updateNotNull(sysUser);
	}

	/**
	 * 插入用户关联职位
	 * 
	 * @param sysUser
	 */
	private void insertUserPost(SysUserVo sysUser) {
		Date now = DateUtil.now();
		for (Long postId : sysUser.getPostIds()) {
			SysUserPost userPost = new SysUserPost();
			userPost.setPostId(postId);
			userPost.setUserId(sysUser.getUserId());
			userPost.setCreateTime(now);
			sysUserPostMapper.insertSelective(userPost);
		}
	}

	/**
	 * 新增用户角色信息
	 * 
	 * @param sysUser
	 *            用户对象
	 */
	public void insertUserRole(SysUserVo sysUser) {
		Long[] roles = sysUser.getRoleIds();
		if (StringUtils.isNotNull(roles)) {
			// 新增用户与角色管理
			Date now = DateUtil.now();
			for (Long roleId : roles) {
				SysUserRole userRole = new SysUserRole();
				userRole.setUserId(sysUser.getUserId());
				userRole.setRoleId(roleId);
				userRole.setCreateTime(now);
				sysUserRoleMapper.insertSelective(userRole);
			}
		}
	}

	@Override
	public boolean isAdmin(Long userId) {
		SysUser sysUser = this.selectByKey(userId);
		return sysUser != null && "00".equals(sysUser.getUserType());
	}

	/**
	 * 校验登录名称是否唯一
	 * 
	 * @param loginName
	 *            用户名
	 * @return
	 */
	@Override
	public String checkLoginNameUnique(String loginName) {
		Example example = new Example(SysUser.class);
		example.createCriteria().andEqualTo("userName", loginName);
		int count = this.countByExample(example);
		if (count > 0) {
			return "1";
		}
		return "0";
	}

	/**
	 * 校验用户名称是否唯一
	 *
	 * @param phoneNum
	 *            用户信息
	 * @return
	 */
	@Override
	public String checkPhoneUnique(String phoneNum) {
		Example example = new Example(SysUser.class);
		example.createCriteria().andEqualTo("phonenumber", phoneNum);
		int count = this.countByExample(example);
		if (count > 0) {
			return "1";
		}
		return "0";
	}

	@Override
	public String checkEmailUnique(String email) {
		Example example = new Example(SysUser.class);
		example.createCriteria().andEqualTo("email", email);
		int count = this.countByExample(example);
		if (count > 0) {
			return "1";
		}
		return "0";
	}

	@Override
	public SysUserVo selectUserById(Long userId) {
		SysUser sysUser = this.selectByKey(userId);
		SysDept sysDept = sysDeptMapper.selectByPrimaryKey(sysUser.getDeptId());
		SysUserVo sysUserVo = new SysUserVo();
		BeanUtils.copyBeanProp(sysUserVo, sysUser);
		sysUserVo.setDept(sysDept);
		return sysUserVo;
	}

	@Override
	public void changeStatus(SysUserVo sysUser) {
		this.updateUser(sysUser);
	}

	@Override
	public void updateUserRole(Long userId, Long[] roleIds) {
		// 删除用户与角色关联
		Example example = new Example(SysUserRole.class);
		example.createCriteria().andEqualTo("userId", userId);
		sysUserRoleMapper.deleteByExample(example);
		// 新增用户与角色管理
		for (Long roleId : roleIds) {
			SysUserRole userRole = new SysUserRole();
			userRole.setCreateTime(DateUtil.now());
			userRole.setRoleId(roleId);
			userRole.setUserId(userId);
			sysUserRoleMapper.insertSelective(userRole);
		}
	}

	@Override
	public List<SysUserRole> listRealRole(Long userId) {
		// 获取用户与角色关联
		Example example = new Example(SysUserRole.class);
		example.createCriteria().andEqualTo("userId", userId);
		return sysUserRoleMapper.selectByExample(example);
	}

	@Override
	public void resetUserPwd(SysUser sysUser) {
		// 一个Byte占两个字节，此处生成的3字节，字符串长度为6
		sysUser.setUpdateTime(DateUtil.now());
		this.updateNotNull(sysUser);
	}

	@Override
	public void resetUserPwd(String userName, String newPassword) {
		SysUser sysUser = new SysUser();
		// 一个Byte占两个字节，此处生成的3字节，字符串长度为6
		sysUser.setUpdateTime(DateUtil.now());
		sysUser.setPassword(newPassword);

		Example example = new Example(SysUser.class);
		example.createCriteria().andEqualTo("userName", userName);
		this.updateNotNullByExample(sysUser, example);
	}

	/**
	 * 查询用户所属角色组
	 * 
	 * @param userName
	 *            用户ID
	 * @return 结果
	 */
	@Override
	public String selectUserRoleGroup(String userName) {
		List<SysRole> list = extSysUserMapper.selectRolesByUserName(userName);
		StringBuffer idsStr = new StringBuffer();
		for (SysRole role : list) {
			idsStr.append(role.getRoleName()).append(",");
		}
		if (StringUtils.isNotEmpty(idsStr.toString())) {
			return idsStr.substring(0, idsStr.length() - 1);
		}
		return idsStr.toString();
	}

	/**
	 * 查询用户所属岗位组
	 * 
	 * @param userName
	 *            用户ID
	 * @return 结果
	 */
	@Override
	public String selectUserPostGroup(String userName) {
		List<SysPost> list = extSysUserMapper.selectPostsByUserName(userName);
		StringBuffer idsStr = new StringBuffer();
		for (SysPost post : list) {
			idsStr.append(post.getPostName()).append(",");
		}
		if (StringUtils.isNotEmpty(idsStr.toString())) {
			return idsStr.substring(0, idsStr.length() - 1);
		}
		return idsStr.toString();
	}

	@Override
	public SysUserVo selectUserByUserName(String userName) {
		return extSysUserMapper.selectUserByUserName(userName);
	}

	@Override
	public void deleteUserByIds(Long[] userIds) {
		if (userIds == null || userIds.length == 0) {
			return;
		}
		Example example = new Example(SysUser.class);
		List<Long> userIdList = Arrays.asList(userIds);
		example.createCriteria().andIn("userId", userIdList);
		this.deleteBy(example);
	}

	@Override
	public boolean updateUserAvatar(String userName, String avatar) {
		SysUser sysUser = new SysUser();
		sysUser.setUpdateTime(DateUtil.now());
		sysUser.setAvatar(avatar);

		Example example = new Example(SysUser.class);
		example.createCriteria().andEqualTo("userName", userName);
		int result = this.updateNotNullByExample(sysUser, example);

		return result > 0;
	}

	@Override
	public Long getTenantId(Long userId){
		SysUser sysUser = this.selectByKey(userId);
		Long deptId = sysUser.getDeptId();
		SysDept sysDept = sysDeptMapper.selectByPrimaryKey(deptId);
		return sysDept.getTenantId();
	}
}

package com.pimee.service.sys;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.IBaseService;
import com.pimee.model.SysTenant;
import com.pimee.model.vo.SysTenantVo;

import java.util.List;
import java.util.Map;

public interface ISysTenantService extends IBaseService<SysTenant> {

    /**
     * 分页列表
     * @param param
     * @return
     */
    public PageInfo<Map<String, Object>> pageList(Map<String, Object> param);

    /**
     * 查询租户列表
     * @return
     */
    List<SysTenant> listTenant();

    /**
     * 插入
     * @param param
     */
    public void insert(SysTenant param);

    /**
     * 更新
     * @param param
     */
    public void update(SysTenant param);

    /**
     * 批量删除
     * @param ids
     */
    public void deleteByIds(Long[] ids);

    /**
     * 租户续费
     * @param param
     */
    void renew(SysTenantVo param);
}

package com.pimee.service.sys;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.IBaseService;
import com.pimee.model.SysTenantRenewLog;

import java.util.Map;

public interface ISysTenantRenewLogService extends IBaseService<SysTenantRenewLog> {

    /**
     * 分页列表
     * @param param
     * @return
     */
    public PageInfo<Map<String, Object>> pageList(Map<String, Object> param);

    /**
     * 插入
     * @param param
     */
    public void insert(SysTenantRenewLog param);

    /**
     * 批量删除
     * @param ids
     */
    public void deleteByIds(Long[] ids);

}

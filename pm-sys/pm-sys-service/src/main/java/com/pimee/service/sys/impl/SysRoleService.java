package com.pimee.service.sys.impl;

import com.github.pagehelper.PageInfo;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.service.impl.BaseService;
import com.pimee.common.utils.DateUtil;
import com.pimee.common.utils.StringUtils;
import com.pimee.mapper.SysRoleDeptMapper;
import com.pimee.mapper.SysRoleMenuMapper;
import com.pimee.mapper.ext.ExtSysRoleMapper;
import com.pimee.model.SysRole;
import com.pimee.model.SysRoleDept;
import com.pimee.model.SysRoleMenu;
import com.pimee.service.sys.ISysRoleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.*;

@Slf4j
@Service
public class SysRoleService extends BaseService<SysRole> implements ISysRoleService {
    @Resource
    private ExtSysRoleMapper extSysRoleMapper;
    @Resource
    private SysRoleDeptMapper sysRoleDeptMapper;
    @Resource
    private SysRoleMenuMapper sysRoleMenuMapper;

    @Override
    public PageInfo<SysRole> pageList(Map<String, Object> params) {
        log.info("===> 角色列表分页查询...");
        startPage(params);// 设置分页
        List<SysRole> list = extSysRoleMapper.listRole(params);
        return new PageInfo<>(list);
    }

    @Override
    public List<Long> selectRoleIdsByUserId(Long userId) {
        List<SysRole> roles = extSysRoleMapper.selectRolesByUserId(userId);
        List<Long> roleIds = new ArrayList<>();
        for (SysRole role : roles) {
            if (StringUtils.isNotNull(role)) {
                roleIds.add(role.getRoleId());
            }
        }
        return roleIds;
    }

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    @Override
    public List<SysRole> selectRolesByUserId(Long userId) {
        List<SysRole> userRoles = extSysRoleMapper.selectRolesByUserId(userId);
        List<SysRole> roles = this.listAll();
        for (SysRole role : roles) {
            for (SysRole userRole : userRoles) {
                if (role.getRoleId().longValue() == userRole.getRoleId().longValue()) {
                    role.setFlag(true);
                    break;
                }
            }
        }
        return roles;
    }

    @Override
    public void insertRole(SysRole param) {
        Date now = DateUtil.now();
        param.setCreateTime(now);
        param.setUpdateTime(now);
        this.saveNotNull(param);

        insertRoleMenu(param);
    }

    /**
     * 新增角色菜单信息
     *
     * @param role 角色对象
     */
    public void insertRoleMenu(SysRole role) {
        // 新增角色与菜单
        List<SysRoleMenu> list = new ArrayList<SysRoleMenu>();
        for (Long menuId : role.getMenuIds()) {
            SysRoleMenu rm = new SysRoleMenu();
            rm.setRoleId(role.getRoleId());
            rm.setMenuId(menuId);
            list.add(rm);
        }
        if (CollectionUtils.isNotEmpty(list)) {
            extSysRoleMapper.batchRoleMenu(list);
        }
    }

    @Override
    public void updateRole(SysRole param) {
        Date now = DateUtil.now();
        param.setUpdateTime(now);
        param.setTenantId(null); // 租户ID保持原来的 暂时处理  后面把角色修改和菜单授权分离即可解决 <TODO/>
        this.updateNotNull(param);

        if (CollectionUtils.isNotEmpty(param.getMenuIds())) {
            this.updateRoleMenu(param.getRoleId(), param.getMenuIds());
        }

    }

    @Override
    public void updateRoleMenu(Long roleId, List<Long> menuIds) {
        // 删除角色菜单
        Example example = new Example(SysRoleMenu.class);
        example.createCriteria().andEqualTo("roleId", roleId);
        sysRoleMenuMapper.deleteByExample(example);

        List<SysRoleMenu> list = new ArrayList<SysRoleMenu>();
        for (Long menuId : menuIds) {
            SysRoleMenu sysRoleMenu = new SysRoleMenu();
            sysRoleMenu.setRoleId(roleId);
            sysRoleMenu.setMenuId(Long.valueOf(menuId));
            list.add(sysRoleMenu);
        }
        if (CollectionUtils.isNotEmpty(list)) {
            extSysRoleMapper.batchRoleMenu(list);
        }
    }

    @Override
    public void deleteRole(Long roleId) {
        // 删除角色菜单
        Example example = new Example(SysRoleMenu.class);
        example.createCriteria().andEqualTo("roleId", roleId);
        sysRoleMenuMapper.deleteByExample(example);

        this.deleteByKey(roleId);
    }

    @Override
    public void deleteRoleByKeys(Long[] roleIds) {
        List<Long> roleKeys = Arrays.asList(roleIds);
        Example example = new Example(SysRoleMenu.class);
        example.createCriteria().andIn("roleId", roleKeys);
        sysRoleMenuMapper.deleteByExample(example);

        Example roleExample = new Example(SysRole.class);
        roleExample.createCriteria().andIn("roleId", roleKeys);
        this.deleteBy(roleExample);
    }

    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    @Override
    public Set<String> selectRolePermissionByUserId(Long userId) {
        List<SysRole> perms = extSysRoleMapper.selectRolesByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (SysRole perm : perms) {
            if (StringUtils.isNotNull(perm)) {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 校验角色是否允许操作
     *
     * @param role 角色信息
     */
    @Override
    public void checkRoleAllowed(SysRole role) {
        if (StringUtils.isNotNull(role.getRoleId()) && role.isAdmin()) {
            throw new BusinessException(500, "不允许操作超级管理员角色");
        }
    }

    @Override
    @Transactional
    public void authDataScope(SysRole role) {
        // 修改角色信息
        this.updateRole(role);

        // 删除角色与部门关联
        Example example = new Example(SysRoleDept.class);
        example.createCriteria().andEqualTo("roleId", role.getRoleId());
        sysRoleDeptMapper.deleteByExample(example);
        // 新增角色和部门信息（数据权限）
        insertRoleDept(role);
    }

    private void insertRoleDept(SysRole role) {
        // 新增角色与部门（数据权限）管理
        List<SysRoleDept> list = new ArrayList<SysRoleDept>();
        for (Long deptId : role.getDeptIds()) {
            SysRoleDept rd = new SysRoleDept();
            rd.setRoleId(role.getRoleId());
            rd.setDeptId(deptId);
            list.add(rd);
        }
        if (list.size() > 0) {
            extSysRoleMapper.batchRoleDept(list);
        }
    }

    @Override
    public Long getTenantIdByRoleId(Long roleId){
        SysRole sysRole = this.selectByKey(roleId);
        return sysRole.getTenantId();
    }

}

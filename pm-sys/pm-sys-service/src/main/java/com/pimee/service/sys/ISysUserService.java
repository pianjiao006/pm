package com.pimee.service.sys;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.IBaseService;
import com.pimee.model.SysUser;
import com.pimee.model.SysUserRole;
import com.pimee.model.vo.SysUserVo;

public interface ISysUserService extends IBaseService<SysUser> {

	SysUser selectOneByAccount(String account);

	/**
	 * 更新用户信息
	 * 
	 * @param sysUser
	 */
	void updateUser(SysUserVo sysUser);

	/**
	 * 校验用户是否超级管理员
	 * 
	 * @param userId
	 * @return
	 */
	boolean isAdmin(Long userId);

	/**
	 * 用户分页列表
	 * 
	 * @param params
	 * @return
	 */
	public PageInfo<SysUserVo> pageList(Map<String, Object> params);

	/**
	 * 校验登录账号是否存在
	 * 
	 * @param loginName
	 * @return
	 */
	String checkLoginNameUnique(String loginName);

	/**
	 * 校验手机号码是否唯一
	 *
	 * @param phoneNum
	 *            用户信息
	 * @return 结果
	 */
	public String checkPhoneUnique(String phoneNum);

	/**
	 * 邮箱唯一性
	 * @param phoneNum
	 * @return
	 */
	public String checkEmailUnique(String phoneNum);

	/**
	 * 根据用户信息
	 * 
	 * @param userId
	 * @return
	 */
	SysUserVo selectUserById(Long userId);

	/**
	 * 添加用户
	 * 
	 * @param sysUser
	 */
	void addUser(SysUserVo sysUser);

	/**
	 * 更新用户状态
	 * 
	 * @param sysUser
	 */
	void changeStatus(SysUserVo sysUser);

	/**
	 * 更新用户角色
	 * 
	 * @param userId
	 * @param roleIds
	 */
	void updateUserRole(Long userId, Long[] roleIds);

	/**
	 * 获取用户关联的角色
	 * 
	 * @param userId
	 * @return
	 */
	List<SysUserRole> listRealRole(Long userId);

	/**
	 * 重置密码
	 * 
	 * @param sysUser
	 */
	void resetUserPwd(SysUser sysUser);

	public void resetUserPwd(String userName, String newPassword);

	/**
	 * 查询用户所属角色组
	 * 
	 * @param userName
	 * @return
	 */
	String selectUserRoleGroup(String userName);

	/**
	 * 查询用户所属岗位组
	 * 
	 * @param userName
	 * @return
	 */
	String selectUserPostGroup(String userName);

	/**
	 * 根据账号获取用户关联信息
	 * 
	 * @param userName
	 * @return
	 */
	SysUserVo selectUserByUserName(String userName);

	/**
	 * 删除用户信息
	 * 
	 * @param userIds
	 */
	void deleteUserByIds(Long[] userIds);

	/**
	 * 更新头像信息
	 * @param userName
	 * @param avatar
	 * @return
	 */
    boolean updateUserAvatar(String userName, String avatar);

	/**
	 * 获取租户ID
	 * @param userId
	 * @return
	 */
	public Long getTenantId(Long userId);
}

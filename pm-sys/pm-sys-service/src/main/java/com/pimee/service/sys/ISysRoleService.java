package com.pimee.service.sys;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.IBaseService;
import com.pimee.model.SysRole;

public interface ISysRoleService extends IBaseService<SysRole> {
	/**
	 * 获取用户关联角色id
	 * 
	 * @param userId
	 * @return
	 */
	List<Long> selectRoleIdsByUserId(Long userId);

	/**
	 * 获取用户关联角色
	 * 
	 * @param userId
	 * @return
	 */
	List<SysRole> selectRolesByUserId(Long userId);

	/**
	 * 角色分页列表
	 * 
	 * @param params
	 * @return
	 */
	PageInfo<SysRole> pageList(Map<String, Object> params);

	/**
	 * 新增角色
	 * 
	 * @param param
	 */
	void insertRole(SysRole param);

	/**
	 * 更新角色
	 * 
	 * @param param
	 */
	void updateRole(SysRole param);

	/**
	 * 更新角色菜单
	 * 
	 * @param roleId
	 * @param menuIds
	 */
	void updateRoleMenu(Long roleId, List<Long> menuIds);

	/**
	 * 删除角色
	 * 
	 * @param roleId
	 */
	void deleteRole(Long roleId);

	/**
	 * 删除角色
	 * 
	 * @param roleIds
	 */
	void deleteRoleByKeys(Long[] roleIds);

	/**
	 * 获取角色权限列表
	 * 
	 * @param userId
	 *            用户ID
	 * @return 权限列表
	 */
	public Set<String> selectRolePermissionByUserId(Long userId);

	/**
	 * 校验角色是否允许操作
	 * 
	 * @param role
	 */
	void checkRoleAllowed(SysRole role);

	/**
	 * 数据授权
	 * 
	 * @param role
	 * @return
	 */
	void authDataScope(SysRole role);

	/**
	 * 通过角色ID获取租户ID
	 * @param roleId
	 * @return
	 */
    Long getTenantIdByRoleId(Long roleId);
}

package com.pimee.service.sys.impl;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.impl.BaseService;
import com.pimee.common.utils.DateUtil;
import com.pimee.mapper.common.PublicMapper;
import com.pimee.mapper.ext.ExtSystemCommonMapper;
import com.pimee.model.SysOperLog;
import com.pimee.service.sys.ISysOperLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SysOperLogService extends BaseService<SysOperLog> implements ISysOperLogService {

    @Resource
    private ExtSystemCommonMapper extSystemCommonMapper;
    @Resource
    private PublicMapper publicMapper;

    @Override
    public PageInfo<Map<String, Object>> pageList(Map<String, Object> param) {
        log.info("===> 租户列表分页查询...");
        startPage(param);// 设置分页
        List<Map<String, Object>> list = extSystemCommonMapper.pageOperLogList(param);
        return new PageInfo<>(list);
    }

    @Override
    public void insert(SysOperLog param) {
        Date now = DateUtil.now();
        param.setOperTime(now);
        this.saveNotNull(param);
    }

    @Override
    public void deleteByIds(Long[] ids) {
        if (ids == null || ids.length == 0) {
            return;
        }
        Example example = new Example(SysOperLog.class);
        List<Long> idList = Arrays.asList(ids);
        example.createCriteria().andIn("operId", idList);
        this.deleteBy(example);
    }

    @Override
    public void cleanOperLog() {
        publicMapper.executeSql("truncate table `sys_oper_log`");
    }
}

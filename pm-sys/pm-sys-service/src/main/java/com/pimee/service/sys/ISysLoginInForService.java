package com.pimee.service.sys;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.pimee.model.SysLoginInFor;

/**
 * 系统访问日志情况信息 服务层
 * 
 * @author Bruce Shaw 2020年4月13日 下午4:38:33
 */
public interface ISysLoginInForService {
	/**
	 * 查询系统操作日志集合
	 *
	 * @param param 操作日志对象
	 * @return 操作日志集合
	 */
	public PageInfo<Map<String, Object>> pageList(Map<String, Object> param);
	/**
	 * 新增系统登录日志
	 * 
	 * @param logininfor
	 *            访问日志对象
	 */
	public void insertLoginInFor(SysLoginInFor logininfor);

	/**
	 * 查询系统登录日志集合
	 * 
	 * @param logininfor
	 *            访问日志对象
	 * @return 登录记录集合
	 */
	public List<SysLoginInFor> selectLoginInForList(SysLoginInFor logininfor);

	/**
	 * 批量删除系统登录日志
	 * 
	 * @param infoIds
	 *            需要删除的登录日志ID
	 * @return
	 */
	public int deleteByIds(Long[] infoIds);

	/**
	 * 清空系统登录日志
	 */
	public void cleanLoginInFor();
}

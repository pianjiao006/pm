package com.pimee.service.sys.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.pimee.common.service.impl.BaseService;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;
import com.pimee.common.utils.DateUtil;
import com.pimee.mapper.ext.ExtSysPostMapper;
import com.pimee.model.SysPost;
import com.pimee.service.sys.ISysPostService;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;

@Slf4j
@Service
public class SysPostService extends BaseService<SysPost> implements ISysPostService {

	@Resource
	private ExtSysPostMapper extSysPostMapper;

	@Override
	public List<SysPost> selectPostsByUserId(Long userId) {
		List<SysPost> userPosts = extSysPostMapper.selectPostsByUserId(userId);
		List<SysPost> posts = this.listAll();
		for (SysPost post : posts) {
			for (SysPost userPost : userPosts) {
				if (post.getPostId().longValue() == userPost.getPostId().longValue()) {
					post.setFlag(true);
					break;
				}
			}
		}
		return posts;
	}

	@Override
	public PageInfo<SysPost> pageList(Map<String, Object> params) {
		log.info("===> 岗位列表分页查询...");
		startPage(params);// 设置分页
		List<SysPost> list = extSysPostMapper.pageList(params);
		return new PageInfo<>(list);
	}
	
	public List<Long> selectPostIdsByUserId(Long userId){
		return extSysPostMapper.selectPostIdsByUserId(userId);
	}

	@Override
	public void insertPost(SysPost param) {
		Date now = DateUtil.now();
		param.setCreateTime(now);
		param.setUpdateTime(now);
		this.saveNotNull(param);
	}

	@Override
	public void updatePost(SysPost param) {
		param.setUpdateTime(DateUtil.now());
		this.updateNotNull(param);
	}

	@Override
	public void deleteByIds(Long[] postIds) {
		if (postIds == null || postIds.length == 0) {
			return;
		}
		Example example = new Example(SysPost.class);
		List<Long> postIdList = Arrays.asList(postIds);
		example.createCriteria().andIn("postId", postIdList);
		this.deleteBy(example);
	}
}

package com.pimee.service.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.pimee.model.vo.SysMenuVo;

/**
 * 权限数据处理
 * 
 * @author Bruce Shaw 2020年4月13日 下午5:43:30
 */
public class TreeUtils {
	/**
	 * 根据父节点的ID获取所有子节点
	 * 
	 * @param list
	 *            分类表
	 * @param parentId
	 *            传入的父节点ID
	 * @return String
	 */
	public static List<SysMenuVo> getChildPerms(List<SysMenuVo> list, int parentId) {
		List<SysMenuVo> returnList = new ArrayList<SysMenuVo>();
		for (Iterator<SysMenuVo> iterator = list.iterator(); iterator.hasNext();) {
			SysMenuVo t = (SysMenuVo) iterator.next();
			// 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
			if (t.getParentId() == parentId) {
				recursionFn(list, t);
				returnList.add(t);
			}
		}
		return returnList;
	}

	/**
	 * 递归列表
	 * 
	 * @param list
	 * @param t
	 */
	private static void recursionFn(List<SysMenuVo> list, SysMenuVo t) {
		// 得到子节点列表
		List<SysMenuVo> childList = getChildList(list, t);
		t.setChildren(childList);
		for (SysMenuVo tChild : childList) {
			if (hasChild(list, tChild)) {
				// 判断是否有子节点
				Iterator<SysMenuVo> it = childList.iterator();
				while (it.hasNext()) {
					SysMenuVo n = (SysMenuVo) it.next();
					recursionFn(list, n);
				}
			}
		}
	}

	/**
	 * 得到子节点列表
	 */
	private static List<SysMenuVo> getChildList(List<SysMenuVo> list, SysMenuVo t) {

		List<SysMenuVo> tlist = new ArrayList<SysMenuVo>();
		Iterator<SysMenuVo> it = list.iterator();
		while (it.hasNext()) {
			SysMenuVo n = (SysMenuVo) it.next();
			if (n.getParentId().longValue() == t.getMenuId().longValue()) {
				tlist.add(n);
			}
		}
		return tlist;
	}

	List<SysMenuVo> returnList = new ArrayList<SysMenuVo>();

	/**
	 * 根据父节点的ID获取所有子节点
	 * 
	 * @param list
	 *            分类表
	 * @param typeId
	 *            传入的父节点ID
	 * @param prefix
	 *            子节点前缀
	 */
	public List<SysMenuVo> getChildPerms(List<SysMenuVo> list, int typeId, String prefix) {
		if (list == null) {
			return null;
		}
		for (Iterator<SysMenuVo> iterator = list.iterator(); iterator.hasNext();) {
			SysMenuVo node = (SysMenuVo) iterator.next();
			// 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
			if (node.getParentId() == typeId) {
				recursionFn(list, node, prefix);
			}
			// 二、遍历所有的父节点下的所有子节点
			/*
			 * if (node.getParentId()==0) { recursionFn(list, node); }
			 */
		}
		return returnList;
	}

	private void recursionFn(List<SysMenuVo> list, SysMenuVo node, String p) {
		// 得到子节点列表
		List<SysMenuVo> childList = getChildList(list, node);
		if (hasChild(list, node)) {
			// 判断是否有子节点
			returnList.add(node);
			Iterator<SysMenuVo> it = childList.iterator();
			while (it.hasNext()) {
				SysMenuVo n = (SysMenuVo) it.next();
				n.setMenuName(p + n.getMenuName());
				recursionFn(list, n, p + p);
			}
		} else {
			returnList.add(node);
		}
	}

	/**
	 * 判断是否有子节点
	 */
	private static boolean hasChild(List<SysMenuVo> list, SysMenuVo t) {
		return getChildList(list, t).size() > 0 ? true : false;
	}
}

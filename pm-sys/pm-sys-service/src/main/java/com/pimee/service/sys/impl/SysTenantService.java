package com.pimee.service.sys.impl;

import com.github.pagehelper.PageInfo;
import com.pimee.common.aop.annotation.DataScope;
import com.pimee.common.constants.Constants;
import com.pimee.common.core.support.HttpCode;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.service.impl.BaseService;
import com.pimee.common.utils.DateUtil;
import com.pimee.common.utils.security.SecurityUtils;
import com.pimee.common.utils.seq.IdWorker;
import com.pimee.mapper.SysDeptMapper;
import com.pimee.mapper.ext.ExtSysTenantMapper;
import com.pimee.model.SysDept;
import com.pimee.model.SysTenant;
import com.pimee.model.SysTenantRenewLog;
import com.pimee.model.vo.SysTenantVo;
import com.pimee.service.sys.ISysMenuService;
import com.pimee.service.sys.ISysTenantRenewLogService;
import com.pimee.service.sys.ISysTenantService;
import com.pimee.service.sys.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SysTenantService extends BaseService<SysTenant> implements ISysTenantService {

	@Resource
	private ExtSysTenantMapper extSysTenantMapper;
	@Resource
	private SysDeptMapper sysDeptMapper;
	@Resource
	private ISysTenantRenewLogService sysTenantRenewLogService;
	@Resource
	private ISysUserService sysUserService;
	@Resource
	private ISysMenuService sysMenuService;

	@DataScope(deptAlias = "d", userAlias = "u")
	@Override
	public PageInfo<Map<String, Object>> pageList(Map<String, Object> param) {
		log.info("===> 租户列表分页查询...");
		startPage(param);// 设置分页
		List<Map<String, Object>> list = extSysTenantMapper.pageList(param);
		return new PageInfo<>(list);
	}

	@Override
	public List<SysTenant> listTenant() {
		Long userId = SecurityUtils.getUserId();
		Example example = new Example(SysTenant.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("enabled", 0);
		Long tenantId = sysUserService.getTenantId(userId);
		if(!sysUserService.isAdmin(userId)){
			criteria.andEqualTo("tenantId", tenantId);
		}
		return this.selectByExample(example);
	}

	@Override
	public void insert(SysTenant param) {
		Date now = DateUtil.now();
		param.setAppId(IdWorker.getId()+"");
		param.setAppSecret(IdWorker.get32UUID());
		param.setCreateTime(now);
		param.setUpdateTime(now);
		this.saveNotNull(param);

		// 插入一个默认的顶级代理
		SysDept dept = new SysDept();
		dept.setParentId(0l);
		dept.setTenantId(param.getTenantId());
		dept.setDeptName(param.getTenantName());
		dept.setLeader(param.getPrincipal());
		dept.setAncestors("0");
		dept.setOrderNum(0);
		dept.setDelFlag("0");
		dept.setPhone(param.getPhone());
		dept.setCreateTime(now);
		dept.setUpdateTime(now);
		dept.setCreateBy(param.getCreateBy());
		dept.setUpdateBy(param.getUpdateBy());
		sysDeptMapper.insertSelective(dept);

		// 初始化一份归属于新租户的订单
		sysMenuService.copyMenuFrom(1l, param.getTenantId());
	}

	@Override
	public void update(SysTenant param) {
		param.setUpdateTime(DateUtil.now());
		this.updateNotNull(param);
	}

	@Override
	public void deleteByIds(Long[] ids) {
		if (ids == null || ids.length == 0) {
			return;
		}

		List<Long> idList = Arrays.asList(ids);
		Example deptExample = new Example(SysDept.class);
		deptExample.createCriteria().andIn("tenantId", idList);
		int deptCount = sysDeptMapper.selectCountByExample(deptExample);
		if (deptCount > 0) {
			throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "当前租户关联部门, 不允许删除");
		}

		Example example = new Example(SysTenant.class);
		example.createCriteria().andIn("tenantId", idList);
		this.deleteBy(example);
	}

	@Override
	public void renew(SysTenantVo param) {
		Integer renewTimeType = param.getRenewTimeType();
		Date startTime = null;
		Date buyTime = null;
		Date expireTime = null;
		boolean isExpire = false;
		// 判断是否过期时间是否大于现在时间 如果大于，那就属于过期续费，购买时间要重置为现在现在时间
		if(DateUtil.compareDate(param.getExpireTime(), DateUtil.now()) < 0) {
			buyTime = DateUtil.now();
			startTime = buyTime;
			param.setBuyTime(buyTime);
			isExpire = true;
		}else{
			startTime = param.getExpireTime();
			expireTime = param.getExpireTime();
			buyTime = param.getBuyTime();
		}

		// 计算过期时间
		Date newExpireTime = null;
		if(renewTimeType == -1){
			newExpireTime = DateUtil.addMonths(startTime, 6); // 添加半年 也就是6个月
		}else{
			newExpireTime = DateUtil.addYears(startTime, renewTimeType);
		}

		param.setEnabled(Integer.parseInt(Constants.SUCCESS)); // 续费成功后 直接把状态更新为正常
		param.setBuyTime(buyTime);
		param.setExpireTime(newExpireTime);
		this.update(param);

		// 判断是否过期续费
		if(isExpire){
			expireTime = newExpireTime;
		}

		// 记录续费日志
		SysTenantRenewLog sysTenantRenewLog = new SysTenantRenewLog();
		sysTenantRenewLog.setTenantId(param.getTenantId());
		sysTenantRenewLog.setCreateBy(param.getCreateBy());
		sysTenantRenewLog.setBuyTime(buyTime);
		sysTenantRenewLog.setExpireTime(expireTime);
		sysTenantRenewLog.setNewExpireTime(newExpireTime);
		sysTenantRenewLogService.insert(sysTenantRenewLog);
	}
}

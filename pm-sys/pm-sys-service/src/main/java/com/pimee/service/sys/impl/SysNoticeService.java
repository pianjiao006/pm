package com.pimee.service.sys.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.pimee.common.core.support.HttpCode;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.service.impl.BaseService;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;
import com.pimee.common.utils.DateUtil;
import com.pimee.mapper.ext.ExtSysNoticeMapper;
import com.pimee.model.SysNotice;
import com.pimee.service.sys.ISysNoticeService;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;

@Slf4j
@Service
public class SysNoticeService extends BaseService<SysNotice> implements ISysNoticeService {

	@Resource
	private ExtSysNoticeMapper extSysNoticeMapper;

	@Override
	public PageInfo<SysNotice> pageList(Map<String, Object> params) {
		log.info("===> 通知列表分页查询...");
		startPage(params);// 设置分页
		List<SysNotice> list = extSysNoticeMapper.pageList(params);
		return new PageInfo<>(list);
	}
	
	@Override
	public void insertNotice(SysNotice param) {
		Date now = DateUtil.now();
		param.setCreateTime(now);
		param.setUpdateTime(now);
		this.saveNotNull(param);
	}

	@Override
	public void updateNotice(SysNotice param) {
		param.setUpdateTime(DateUtil.now());
		this.updateNotNull(param);
	}

	@Override
	public void deleteByIds(Long[] noticeIds) {
		if (noticeIds == null || noticeIds.length == 0) {
			throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), "没有传入任何要删除的标识");
		}
		Example example = new Example(SysNotice.class);
		List<Long> idList = Arrays.asList(noticeIds);
		example.createCriteria().andIn("noticeId", idList);
		this.deleteBy(example);
	}
}

package com.pimee.mapper;

import com.pimee.model.SysRoleDept;
import tk.mybatis.mapper.common.Mapper;

public interface SysRoleDeptMapper extends Mapper<SysRoleDept> {
}
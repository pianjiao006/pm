package com.pimee.mapper.ext;

import java.util.List;
import java.util.Map;

import com.pimee.model.SysConfig;

public interface ExtSysConfigMapper {
	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	public List<SysConfig> pageList(Map<String, Object> params);
}

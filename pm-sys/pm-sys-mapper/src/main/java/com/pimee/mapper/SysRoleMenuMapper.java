package com.pimee.mapper;

import com.pimee.model.SysRoleMenu;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SysRoleMenuMapper extends Mapper<SysRoleMenu> {

}
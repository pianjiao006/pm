package com.pimee.mapper;

import com.pimee.model.SysPost;
import tk.mybatis.mapper.common.Mapper;

public interface SysPostMapper extends Mapper<SysPost> {
}
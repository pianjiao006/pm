package com.pimee.mapper.common;

import java.util.List;
import java.util.Map;

/**
 * 执行动态脚本的公共类
 */
public interface PublicMapper {
    /**
     * 执行获取一条记录
     *
     * @param sql
     * @return
     */
    Map<String, Object> executeSelectOneSql(String sql);

    /**
     * 执行获取的记录总数
     *
     * @param sql
     * @return
     */
    void executeSql(String sql);

    /**
     * 执行脚本返回所有的记录
     *
     * @param sql
     * @return
     */
    List<Map<String, Object>> executeSelectSql(String sql);
}

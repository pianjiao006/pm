package com.pimee.mapper;

import com.pimee.model.SysDept;
import tk.mybatis.mapper.common.Mapper;

public interface SysDeptMapper extends Mapper<SysDept> {
}
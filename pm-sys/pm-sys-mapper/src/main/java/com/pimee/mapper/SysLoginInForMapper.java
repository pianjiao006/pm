package com.pimee.mapper;

import com.pimee.model.SysLoginInFor;
import tk.mybatis.mapper.common.Mapper;

public interface SysLoginInForMapper extends Mapper<SysLoginInFor> {
}
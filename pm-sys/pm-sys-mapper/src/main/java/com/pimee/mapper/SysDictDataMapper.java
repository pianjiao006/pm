package com.pimee.mapper;

import com.pimee.model.SysDictData;
import tk.mybatis.mapper.common.Mapper;

public interface SysDictDataMapper extends Mapper<SysDictData> {
}
package com.pimee.mapper.ext;

import java.util.List;
import java.util.Map;

import com.pimee.model.SysNotice;

public interface ExtSysNoticeMapper {
	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	public List<SysNotice> pageList(Map<String, Object> params);
}

package com.pimee.mapper.ext;

import java.util.List;
import java.util.Map;

import com.pimee.model.SysDept;
import org.apache.ibatis.annotations.Param;

public interface ExtSysDeptMapper {
	
	/**
	 * dept列表
	 * @return
	 */
	List<SysDept> listDept(Map<String, Object> params);

	/**
	 * 根据角色表示 获取代理列表
	 * @param roleId
	 * @param deptCheckStrictly
	 * @return
	 */
	public List<Integer> selectDeptListByRoleId(@Param("roleId") Long roleId, @Param("deptCheckStrictly") boolean deptCheckStrictly);

	/**
	 * 根据ID查询所有子部门（正常状态）
	 *
	 * @param deptId 部门ID
	 * @return 子部门数
	 */
	public int selectNormalChildrenDeptById(@Param("deptId") Long deptId);

	/**
	 *
	 * 更新自己和子级的租户ID
	 * @param param
	 */
	public void updateChildTenant(SysDept param);
}

package com.pimee.mapper.ext;

import java.util.List;
import java.util.Map;

import com.pimee.model.SysUser;
import org.apache.ibatis.annotations.Param;

import com.pimee.model.vo.SysMenuVo;

public interface ExtSysMenuMapper {

	/**
	 * 获取用户菜单
	 * 
	 * @param userId
	 * @param tenantId
	 * @return
	 */
	public List<SysMenuVo> selectMenusByUserId(@Param("userId") Long userId, @Param("tenantId") Long tenantId);

	/**
	 * 获取全部正常的菜单
	 * 
	 * @return
	 */
	public List<SysMenuVo> selectMenuNormalAll(@Param("tenantId") Long tenantId);

	/**
	 * 根据用户ID查询权限
	 * 
	 * @param userId
	 *            用户ID
	 * @return 权限列表
	 */
	public List<String> selectPermsByUserId(Long userId);

	/**
	 * 查询菜单列表
	 * 
	 * @param params
	 * @return
	 */
	public List<SysMenuVo> selectMenuList(Map<String, Object> params);

	/**
	 * 查询某个用户的菜单列表
	 * 
	 * @param params
	 * @return
	 */
	public List<SysMenuVo> selectMenuListByUserId(Map<String, Object> params);

	/**
	 * 获取角色菜单树
	 * 
	 * @param roleId
	 * @return
	 */
	public List<String> selectMenuTree(Long roleId);

	/**
	 * 获取角色对应的资源
	 * 
	 * @param roleId
	 * @return
	 */
	public List<Integer> selectMenuListByRoleId(@Param("roleId")Long roleId, @Param("menuCheckStrictly")boolean menuCheckStrictly);
}

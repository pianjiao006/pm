package com.pimee.support.security.service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSONObject;
import com.pimee.common.core.support.HttpCode;
import com.pimee.support.security.filter.NameHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.pimee.common.constants.Constants;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.exception.CaptchaException;
import com.pimee.common.exception.CaptchaExpireException;
import com.pimee.common.exception.UserPasswordNotMatchException;
import com.pimee.common.utils.MessageUtils;
import com.pimee.common.utils.RedisUtil;
import com.pimee.support.manager.AsyncManager;
import com.pimee.support.manager.factory.AsyncFactory;
import com.pimee.common.utils.security.LoginUser;

/**
 * 登录校验方法
 *
 * @author Bruce Shaw 2020年4月13日 下午2:58:41
 */
@Component
public class SysLoginService {
    @Resource
    private TokenService tokenService;
    @Resource
    private AuthenticationManager authenticationManager;
    @Resource
    private RedisUtil<String> redisUtil;

    /**
     * 登录验证
     *
     * @param userName 用户名
     * @param password 密码
     * @param uuid     唯一标识
     * @return 结果
     */
    public String login(String userType, String userName, String password, String code, String uuid) {
        /**************** 添加对前端用户的支持 *************/
        NameHelper nameHelper=new NameHelper();
        nameHelper.setName(userName);
        nameHelper.setUserType(userType);

        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;
        String captcha = redisUtil.string_get(verifyKey);
        redisUtil.delete(verifyKey);
		String errMsg = "";
        if (captcha == null) {
			errMsg = MessageUtils.message("user.jcaptcha.expire");
            AsyncManager.me().execute(AsyncFactory.recordLoginInFor(userName, Constants.LOGIN_FAIL, errMsg));
            throw new CaptchaExpireException(errMsg);
        }
        if (!code.equalsIgnoreCase(captcha)) {
            errMsg = MessageUtils.message("user.jcaptcha.error");
            AsyncManager.me().execute(AsyncFactory.recordLoginInFor(userName, Constants.LOGIN_FAIL, errMsg));
            throw new CaptchaException(errMsg);
        }
        // 用户验证
        Authentication authentication = null;
        try {
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
            authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(JSONObject.toJSONString(nameHelper), password));
        } catch (Exception e) {
            if (e instanceof BadCredentialsException) {
                errMsg = MessageUtils.message("user.password.not.match");
                AsyncManager.me().execute(AsyncFactory.recordLoginInFor(userName, Constants.LOGIN_FAIL, errMsg));
                throw new UserPasswordNotMatchException(HttpCode.INTERNAL_SERVER_ERROR.value(), errMsg);
            } else {
                AsyncManager.me()
                        .execute(AsyncFactory.recordLoginInFor(userName, Constants.LOGIN_FAIL, e.getMessage()));
                throw new BusinessException(e.getMessage());
            }
        }
		errMsg = MessageUtils.message("user.login.success");
        AsyncManager.me().execute(AsyncFactory.recordLoginInFor(userName, Constants.LOGIN_SUCCESS, errMsg));
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        // 登录成功，生成token
        return tokenService.createToken(loginUser);
    }
}

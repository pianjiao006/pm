package com.pimee.job.service.impl;

import com.github.pagehelper.PageInfo;
import com.pimee.common.service.impl.BaseService;
import com.pimee.job.mapper.ext.ExtSysJobLogMapper;
import com.pimee.job.model.SysJobLog;
import com.pimee.job.service.ISysJobLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SysJobLogService extends BaseService<SysJobLog> implements ISysJobLogService {
	
	@Autowired
	private ExtSysJobLogMapper extSysJobLogMapper;
	
	@Override
	public PageInfo<SysJobLog> pageList(Map<String, Object> params) {
		log.info("===> 任务列表分页查询...");
		startPage(params);// 设置分页
		List<SysJobLog> list = extSysJobLogMapper.pageList(params);
		return new PageInfo<>(list);
	}

}

package com.pimee.job;

import com.pimee.common.constants.Constants;
import com.pimee.common.constants.Constants.JobStatus;
import com.pimee.common.utils.DateUtil;
import com.pimee.common.utils.ExceptionUtil;
import com.pimee.common.utils.SpringContextUtil;
import com.pimee.common.utils.StringUtils;
import com.pimee.common.utils.bean.BeanUtils;
import com.pimee.job.model.SysJob;
import com.pimee.job.model.SysJobLog;
import com.pimee.job.service.impl.SysJobLogService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * 抽象quartz调用
 * 
 * @description
 * @author Bruce Shaw 2020年3月18日 下午5:37:51
 */
public abstract class AbstractQuartzJob implements org.quartz.Job {
	private static final Logger log = LoggerFactory.getLogger(AbstractQuartzJob.class);

	/**
	 * 线程本地变量
	 */
	private static ThreadLocal<Date> threadLocal = new ThreadLocal<>();

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		SysJob job = new SysJob();
		BeanUtils.copyBeanProp(job, context.getMergedJobDataMap().get(Constants.Schedule.TASK_PROPERTIES));
		try {
			before(context, job);
			if (job != null) {
				doExecute(context, job);
			}
			after(context, job, null);
		} catch (Exception e) {
			log.error("任务执行异常  - ：", e);
			after(context, job, e);
		}
	}

	/**
	 * 执行前
	 *
	 * @param context
	 *            工作执行上下文对象
	 * @param job
	 *            系统计划任务
	 */
	protected void before(JobExecutionContext context, SysJob job) {
		threadLocal.set(new Date());
	}

	/**
	 * 执行后
	 *
	 * @param context
	 *            工作执行上下文对象
	 * @param job
	 *            系统计划任务
	 */
	protected void after(JobExecutionContext context, SysJob job, Exception e) {
		Date startTime = threadLocal.get();
		threadLocal.remove();

		final SysJobLog jobLog = new SysJobLog();
		jobLog.setJobName(job.getJobName());
		jobLog.setJobGroup(job.getJobGroup());
		jobLog.setInvokeTarget(job.getInvokeTarget());
		long runMs = DateUtil.now().getTime() - startTime.getTime();
		jobLog.setJobMessage(jobLog.getJobName() + " 总共耗时：" + runMs + "毫秒");
		if (e != null) {
			jobLog.setStatus(JobStatus.FAIL.getCode());
			String errorMsg = StringUtils.substring(ExceptionUtil.getStackTraceAsString(e), 0, 2000);
			jobLog.setExceptionInfo(errorMsg);
		} else {
			jobLog.setStatus(JobStatus.SUC.getCode());
		}

		// 写入数据库当中
		SpringContextUtil.getBean(SysJobLogService.class).saveNotNull(jobLog);
	}

	/**
	 * 执行方法，由子类重载
	 *
	 * @param context
	 *            工作执行上下文对象
	 * @param job
	 *            系统计划任务
	 * @throws Exception
	 *             执行过程中的异常
	 */
	protected abstract void doExecute(JobExecutionContext context, SysJob job) throws Exception;
}
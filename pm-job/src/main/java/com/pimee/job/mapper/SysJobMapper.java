package com.pimee.job.mapper;

import com.pimee.job.model.SysJob;
import tk.mybatis.mapper.common.Mapper;

public interface SysJobMapper extends Mapper<SysJob> {
}
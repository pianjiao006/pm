package com.pimee.job.mapper.ext;

import com.pimee.job.model.SysJob;

import java.util.List;
import java.util.Map;

public interface ExtSysJobMapper {
	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	public List<SysJob> pageList(Map<String, Object> params);
}

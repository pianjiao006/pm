package com.pimee.job.mapper.ext;

import com.pimee.job.model.SysJobLog;

import java.util.List;
import java.util.Map;

public interface ExtSysJobLogMapper {
	/**
	 * 分页列表
	 * 
	 * @param params
	 * @return
	 */
	public List<SysJobLog> pageList(Map<String, Object> params);
}

package com.pimee.job.util;

import com.pimee.job.AbstractQuartzJob;
import com.pimee.job.model.SysJob;
import org.quartz.JobExecutionContext;

/**
 * 定时任务处理（允许并发执行）
 * 
 * @author Bruce Shaw 2020年2月5日 下午9:21:18
 *
 */
public class QuartzJobExecution extends AbstractQuartzJob {
	@Override
	protected void doExecute(JobExecutionContext context, SysJob job) throws Exception {
		com.pimee.job.util.JobInvokeUtil.invokeMethod(job);
	}
}

package com.pimee.job.util;

import com.pimee.common.exception.BusinessException;
import com.pimee.job.model.SysJob;
import com.pimee.common.constants.Constants.*;
import org.quartz.*;

/**
 * 定时任务工具
 * 
 * @author ruoyi
 *
 */
public class ScheduleUtils {

	/**
	 * 得到quartz任务对象
	 *
	 * @param job
	 *            执行计划
	 * @return 具体执行任务对象
	 */
	private static Class<? extends org.quartz.Job> getQuartzJobClass(SysJob job) {
		boolean isConcurrent = "0".equals(job.getConcurrent());
		return isConcurrent ? QuartzJobExecution.class : com.pimee.job.util.QuartzDisallowConcurrentExecution.class;
	}

	/**
	 * 构建任务触发对象
	 */
	public static TriggerKey getTriggerKey(Long jobId, String jobGroup) {
		return TriggerKey.triggerKey(Schedule.TASK_CLASS_NAME + jobId, jobGroup);
	}

	/**
	 * 构建任务键对象
	 */
	public static JobKey getJobKey(Long jobId, String jobGroup) {
		return JobKey.jobKey(Schedule.TASK_CLASS_NAME + jobId, jobGroup);
	}

	/**
	 * 创建定时任务
	 */
	public static void createScheduleJob(Scheduler scheduler, SysJob job) throws SchedulerException {
		Class<? extends org.quartz.Job> jobClass = getQuartzJobClass(job);
		// 构建job信息
		Long jobId = job.getJobId();
		String jobGroup = job.getJobGroup();
		JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(getJobKey(jobId, jobGroup)).build();

		// 表达式调度构建器
		CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(job.getCronExpression());
		cronScheduleBuilder = handleCronScheduleMisfirePolicy(job, cronScheduleBuilder);

		// 按新的cronExpression表达式构建一个新的trigger
		CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(getTriggerKey(jobId, jobGroup))
				.withSchedule(cronScheduleBuilder).build();

		// 放入参数，运行时的方法可以获�?
		jobDetail.getJobDataMap().put(Schedule.TASK_PROPERTIES, job);

		// 判断是否存在
		if (scheduler.checkExists(getJobKey(jobId, jobGroup))) {
			// 防止创建时存在数据问�? 先移除，然后在执行创建操�?
			scheduler.deleteJob(getJobKey(jobId, jobGroup));
		}

		scheduler.scheduleJob(jobDetail, trigger);

		// 暂停任务
		if (job.getStatus().equals(ScheduleStatus.PAUSE.getCode())) {
			scheduler.pauseJob(ScheduleUtils.getJobKey(jobId, jobGroup));
		}
	}

	/**
	 * 设置定时任务策略
	 */
	public static CronScheduleBuilder handleCronScheduleMisfirePolicy(SysJob job, CronScheduleBuilder cb) {
		switch (job.getMisfirePolicy()) {
		case Schedule.MISFIRE_DEFAULT:
			return cb;
		case Schedule.MISFIRE_IGNORE_MISFIRES:
			return cb.withMisfireHandlingInstructionIgnoreMisfires();
		case Schedule.MISFIRE_FIRE_AND_PROCEED:
			return cb.withMisfireHandlingInstructionFireAndProceed();
		case Schedule.MISFIRE_DO_NOTHING:
			return cb.withMisfireHandlingInstructionDoNothing();
		default:
			throw new BusinessException("The task misfire policy '" + job.getMisfirePolicy()
					+ "' cannot be used in cron schedule tasks：" + TaskCode.CONFIG_ERROR);
		}
	}
}
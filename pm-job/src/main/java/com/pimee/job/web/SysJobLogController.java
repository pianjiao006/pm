package com.pimee.job.web;

import com.github.pagehelper.PageInfo;
import com.pimee.common.utils.ResultUtil;
import com.pimee.job.model.SysJobLog;
import com.pimee.job.service.ISysJobLogService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 调度日志操作处理
 * 
 * @author Bruce Shaw 2020年2月5日 下午9:56:57
 *
 */
@RestController
@RequestMapping("/monitor/jobLog")
public class SysJobLogController {

	@Resource
	private ISysJobLogService sysJobLogService;

	@PreAuthorize("@ss.hasPermi('monitor:job:list')")
	@GetMapping("/list")
	public Object list(@RequestParam Map<String, Object> param) {
		PageInfo<SysJobLog> pageList = sysJobLogService.pageList(param);
		return ResultUtil.ok(pageList);
	}
}

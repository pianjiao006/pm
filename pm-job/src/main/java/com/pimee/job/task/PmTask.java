package com.pimee.job.task;

import com.pimee.common.utils.StringUtils;
import org.springframework.stereotype.Component;

/**
 * 定时任务调度测试
 * 
 * @author Bruce Shaw 2020年2月5日 下午10:14:19
 *
 */
@Component("pmTask")
public class PmTask {

	public void multipleParams(String s, Boolean b, Long l, Double d, Integer i) {
		System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
	}

	public void executeParams(String params) {
		System.out.println("执行有参方法：" + params);
	}

	public void executeNoParams() {
		System.out.println("执行无参方法");
	}

}

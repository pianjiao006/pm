package com.pimee.generator.model;

import lombok.Data;

@Data
public class FieldInfo {
    private boolean keyFlag; // 是否id标识
    private String columnName; // 数据库字段名称
    private String jdbcType; // 数据库字段类型
    private String propertyType; // 实体属性类型
    private String propertyName; // 实体属性名字
    private String propertyNameCamelWhole; // 实体属性名字,从开始字母，格式化为驼峰
    private String remark; // 备注
}

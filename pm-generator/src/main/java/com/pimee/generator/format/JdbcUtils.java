package com.pimee.generator.format;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class JdbcUtils {

    // 将注册驱动程序放入 静态代码块中，程序一启动的时候执行，只执行一次
    static {
        try {
            // 注册驱动程序
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取 数据库连接对象
     */
    public static Connection getConnection(String url, String userName, String password) {
        try {
            Properties props = new Properties();
            props.setProperty("user", userName);
            props.setProperty("password", password);
            props.setProperty("remarks", "true");//设置可以获取remarks信息
            props.setProperty("useInformationSchema", "true");//设置可以获取tables remarks信息
            // 获取数据库连接对象
            return DriverManager.getConnection(url, props);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * 释放资源
     *
     * @param conn Connection 对象
     * @param stmt PreparedStatement 对象
     * @param rs ResultSet 对象
     */
    public static void close(Connection conn, Statement stmt, ResultSet rs) {
        if (rs != null){
            try {
                rs.close();
            }catch (Exception e){
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }
}

package com.pimee.generator;

import com.pimee.generator.service.GenFileService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GenFileServiceTest {
    @Resource
    private GenFileService genFileService;

    @Test
    public void genFiles() {
        // 代码输出基础路径
        String backBasePath = "F:/project/pm/pm-bs";
        String frontBasePath = "F:/project/pm/pm-vue";
        // 表名，如果是多个直接加上英文的逗号分开
        String tableName = "bs_member";
        genFileService.genFiles(tableName, backBasePath, frontBasePath,false);
    }
}

package com.pimee.web.controller.sys;

import com.pimee.common.core.support.HttpCode;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.utils.ResultUtil;
import com.pimee.service.sys.ISysFileService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;

@RestController
@RequestMapping("/system/file")
public class SysFileController {

    @Resource
    private ISysFileService sysUploadService;
    /**
     * 文件上传
     */
    @PostMapping("/upload")
    public Object imgFile(@RequestParam("file") MultipartFile file) throws IOException {
        String fileName = sysUploadService.uploadFile(file);
        return ResultUtil.ok(fileName);
    }

    /**
     * 文件下载
     * @param fileName
     * @param response
     * @return
     */
    @GetMapping("download")
    public Object downloadFile(@RequestParam String fileName, HttpServletResponse response){
        // 文件名以附件的形式下载
        try {
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
            // 读取文件内容。
            InputStream inputStream = sysUploadService.downloadFile(fileName);
            BufferedInputStream in = new BufferedInputStream(inputStream);// 把输入流放入缓存流
            ServletOutputStream outputStream = response.getOutputStream();
            BufferedOutputStream out = new BufferedOutputStream(outputStream);// 把输出流放入缓存流
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = in.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
            if (out != null) {
                out.flush();
                out.close();
            }
            if (in != null) {
                in.close();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e) {
            throw new BusinessException(HttpCode.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
        return ResultUtil.ok();
    }

    /**
     * 文件删除
     * @param fileName
     * @return
     */
    @DeleteMapping("remove/{fileName}")
    public Object removeFile(@PathVariable String fileName){
        sysUploadService.removeFile(fileName);
        return ResultUtil.ok();
    }

    /**
     * 校验文件是否存在
     */
    @GetMapping("checkExist/{fileName}")
    public Object checkExist(@PathVariable String fileName){
        boolean result = sysUploadService.checkExist(fileName);
        return ResultUtil.ok(result);
    }

}

package com.pimee.web.controller.sys;

import com.pimee.common.constants.Constants;
import com.pimee.common.utils.ResultUtil;
import com.pimee.common.utils.ServletUtils;
import com.pimee.common.utils.security.LoginBody;
import com.pimee.common.utils.security.LoginUser;
import com.pimee.model.SysUser;
import com.pimee.model.vo.SysMenuVo;
import com.pimee.model.vo.SysUserVo;
import com.pimee.service.sys.ISysMenuService;
import com.pimee.support.security.service.SysLoginService;
import com.pimee.support.security.service.SysPermissionService;
import com.pimee.support.security.service.TokenService;
import com.pimee.web.controller.model.AjaxResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

/**
 * 登录验证
 * @author Bruce Shaw
 * 2020年4月17日 上午11:41:36
 */
@RestController
public class SysLoginController {
	@Resource
	private SysLoginService loginService;

	@Resource
	private ISysMenuService sysMenuService;

	@Resource
	private SysPermissionService permissionService;

	@Resource
	private TokenService tokenService;

	/**
	 * 登录方法
	 * 
	 * @param loginBody
	 *            用户
	 * @return 结果
	 */
	@ApiOperation("登录操作")
	@PostMapping("/login")
	public AjaxResult login(@RequestBody LoginBody loginBody, HttpServletRequest request) {
		String userType=request.getHeader("userType");
		AjaxResult ajax = AjaxResult.success();
		// 生成令牌
		String token = loginService.login(userType, loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
				loginBody.getUuid());
		ajax.put(Constants.TOKEN, token);
		return ajax;
	}

	/**
	 * 获取用户信息
	 * 
	 * @return 用户信息
	 */
	@GetMapping("getInfo")
	public AjaxResult getInfo() {
		LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
		SysUserVo userVo = loginUser.getUser();
		// 角色集合
		Set<String> roles = permissionService.getRolePermission1(userVo);
		// 权限集合
		Set<String> permissions = permissionService.getMenuPermission(userVo);
		AjaxResult ajax = AjaxResult.success();
		ajax.put("user", userVo);
		ajax.put("roles", roles);
		ajax.put("permissions", permissions);
		return ajax;
	}

	/**
	 * 获取路由信息
	 * 
	 * @return 路由信息
	 */
	@GetMapping("getRouters")
	public Object getRouters() {
		LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
		// 用户信息
		SysUser user = loginUser.getUser();
		List<SysMenuVo> menus = sysMenuService.selectMenusByUser(user);
		return ResultUtil.ok(sysMenuService.buildMenus(menus));
	}
}

package com.pimee.web.controller.sys;

import com.github.pagehelper.PageInfo;
import com.pimee.common.utils.ResultUtil;
import com.pimee.common.utils.security.SecurityUtils;
import com.pimee.model.SysConfig;
import com.pimee.service.sys.ISysConfigService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/system/config")
public class SysConfigController {

    @Resource
    private ISysConfigService sysConfigService;

    /**
     * 根据参数键名查询参数值
     */
    @GetMapping(value = "/configKey/{configKey}")
    public Object getConfigKey(@PathVariable String configKey) {
        return ResultUtil.ok(sysConfigService.getConfigValue(configKey));
    }


    /**
     * 获取参数配置分页列表
     */
    @PreAuthorize("@ss.hasPermi('system:config:list')")
    @GetMapping("/list")
    public Object list(@RequestParam Map<String, Object> params) {
        PageInfo<SysConfig> pageResult = sysConfigService.pageList(params);
        return ResultUtil.ok(pageResult);
    }

    /**
     * 根据参数配置编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:config:query')")
    @GetMapping(value = "/{configId}")
    public Object getInfo(@PathVariable Long configId) {
        return ResultUtil.ok(sysConfigService.selectByKey(configId));
    }

    /**
     * 新增参数配置
     */
    @PreAuthorize("@ss.hasPermi('system:config:add')")
    @PostMapping
    public Object add(@Validated @RequestBody SysConfig config) {
        config.setCreateBy(SecurityUtils.getUsername());
        sysConfigService.insertConfig(config);
        return ResultUtil.ok();
    }

    /**
     * 修改参数配置
     */
    @PreAuthorize("@ss.hasPermi('system:config:edit')")
    @PutMapping
    public Object edit(@Validated @RequestBody SysConfig config) {
        config.setUpdateBy(SecurityUtils.getUsername());
        sysConfigService.updateConfig(config);
        return ResultUtil.ok();
    }

    /**
     * 删除参数配置
     */
    @PreAuthorize("@ss.hasPermi('system:config:remove')")
    @DeleteMapping("/{configIds}")
    public Object remove(@PathVariable Long[] configIds) {
        sysConfigService.deleteByIds(configIds);
        return ResultUtil.ok();
    }

    /**
     * 清空缓存
     */
    @PreAuthorize("@ss.hasPermi('system:config:remove')")
    @DeleteMapping("/clearCache")
    public Object clearCache() {
        sysConfigService.clearCache();
        return ResultUtil.ok();
    }
}

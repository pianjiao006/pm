package com.pimee.web.controller.sys;

import com.github.pagehelper.PageInfo;
import com.pimee.common.utils.ResultUtil;
import com.pimee.common.utils.security.SecurityUtils;
import com.pimee.model.SysDictData;
import com.pimee.service.sys.ISysDictDataService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/system/dict/data")
public class SysDictDataController {
	@Resource
	private ISysDictDataService sysDictDataService;

	/**
	 * 根据字典类型查询字典数据信息
	 */
	@GetMapping(value = "/type/{dictType}")
	public Object dictType(@PathVariable String dictType) {
		return ResultUtil.ok(sysDictDataService.selectDictDataByType(dictType));
	}

	/**
	 * 获取字典数据分页列表
	 */
	@PreAuthorize("@ss.hasPermi('system:dict:list')")
	@GetMapping("/list")
	public Object list(@RequestParam Map<String, Object> params) {
		PageInfo<SysDictData> pageResult = sysDictDataService.pageList(params);
		return ResultUtil.ok(pageResult);
	}

	/**
	 * 根据字典数据编号获取详细信息
	 */
	@PreAuthorize("@ss.hasPermi('system:dict:query')")
	@GetMapping(value = "/{dictCode}")
	public Object getInfo(@PathVariable Long dictCode) {
		return ResultUtil.ok(sysDictDataService.selectByKey(dictCode));
	}

	/**
	 * 新增字典数据
	 */
	@PreAuthorize("@ss.hasPermi('system:dict:add')")
	@PostMapping
	public Object add(@Validated @RequestBody SysDictData dict) {
		dict.setCreateBy(SecurityUtils.getUsername());
		sysDictDataService.insertDictData(dict);
		return ResultUtil.ok();
	}

	/**
	 * 修改字典数据
	 */
	@PreAuthorize("@ss.hasPermi('system:dict:edit')")
	@PutMapping
	public Object edit(@Validated @RequestBody SysDictData dict) {
		dict.setUpdateBy(SecurityUtils.getUsername());
		sysDictDataService.updateDictData(dict);
		return ResultUtil.ok();
	}

	/**
	 * 删除字典数据
	 */
	@PreAuthorize("@ss.hasPermi('system:dict:remove')")
	@DeleteMapping("/{dictIds}")
	public Object remove(@PathVariable Long[] dictIds) {
		sysDictDataService.deleteByIds(dictIds);
		return ResultUtil.ok();
	}


}
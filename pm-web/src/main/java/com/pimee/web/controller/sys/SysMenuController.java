package com.pimee.web.controller.sys;

import com.google.common.collect.Maps;
import com.pimee.common.utils.ResultUtil;
import com.pimee.common.utils.security.SecurityUtils;
import com.pimee.model.SysMenu;
import com.pimee.model.vo.SysMenuVo;
import com.pimee.service.sys.ISysMenuService;
import com.pimee.service.sys.ISysRoleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 菜单信息
 * 
 * @author Bruce Shaw 2020年4月16日 下午3:15:28
 */
@RestController
@RequestMapping("/system/menu")
public class SysMenuController {
	@Resource
	private ISysMenuService sysMenuService;
	@Resource
	private ISysRoleService sysRoleService;

	/**
	 * 获取菜单列表
	 */
	@PreAuthorize("@ss.hasPermi('system:menu:list')")
	@GetMapping("/list")
	public Object list(@RequestParam Map<String, Object> params) {
		List<SysMenuVo> menus = sysMenuService.listMenu(params);
		return ResultUtil.ok(menus);
	}

	/**
	 * 根据菜单编号获取详细信息
	 */
	@PreAuthorize("@ss.hasPermi('system:menu:query')")
	@GetMapping("/{menuId}")
	public Object getInfo(@PathVariable Long menuId) {
		return ResultUtil.ok(sysMenuService.selectByKey(menuId));
	}

	/**
	 * 获取菜单下拉树列表
	 */
	@GetMapping("/treeSelect")
	public Object treeSelect(@RequestParam Map<String, Object> params) {
		List<SysMenuVo> menus = sysMenuService.listMenu(params);
		return ResultUtil.ok(sysMenuService.buildMenuTreeSelect(menus));
	}

	/**
	 * 动态加载对应租户和角色菜单列表树
	 */
	@GetMapping(value = "/tenantRoleMenuTreeSelect/{roleId}/{tenantId}")
	public Object tenantRoleMenuTreeSelect(@PathVariable("roleId") Long roleId, @PathVariable("tenantId") Long tenantId) {
		Map<String, Object> params = Maps.newHashMap();
		params.put("userId", SecurityUtils.getUserId());
		if(tenantId == null){
			tenantId = sysRoleService.getTenantIdByRoleId(roleId);
		}
		params.put("tenantId", tenantId);
		List<SysMenuVo> menus = sysMenuService.listMenu(params);
		Map<String, Object> result = Maps.newHashMap();
		result.put("checkedKeys", sysMenuService.selectMenuListByRoleId(roleId));
		result.put("menus", sysMenuService.buildMenuTreeSelect(menus));
		return ResultUtil.okMap(result);
	}

	/**
	 * 加载对应角色菜单列表树
	 */
	@GetMapping(value = "/roleMenuTreeSelect/{roleId}")
	public Object roleMenuTreeSelect(@PathVariable("roleId") Long roleId) {
		Map<String, Object> params = Maps.newHashMap();
		params.put("userId", SecurityUtils.getUserId());
		Long tenantId = sysRoleService.getTenantIdByRoleId(roleId);
		params.put("tenantId", tenantId);
		List<SysMenuVo> menus = sysMenuService.listMenu(params);
		Map<String, Object> result = Maps.newHashMap();
		result.put("checkedKeys", sysMenuService.selectMenuListByRoleId(roleId));
		result.put("menus", sysMenuService.buildMenuTreeSelect(menus));
		return ResultUtil.okMap(result);
	}

	/**
	 * 新增菜单
	 */
	@PreAuthorize("@ss.hasPermi('system:menu:add')")
	@PostMapping
	public Object add(@Validated @RequestBody SysMenu menu) {
		menu.setCreateBy(SecurityUtils.getUsername());
		sysMenuService.insertMenu(menu);
		return ResultUtil.ok();
	}

	/**
	 * 修改菜单
	 */
	@PreAuthorize("@ss.hasPermi('system:menu:edit')")
	@PutMapping
	public Object edit(@Validated @RequestBody SysMenu menu) {
		menu.setUpdateBy(SecurityUtils.getUsername());
		sysMenuService.updateMenu(menu);
		return ResultUtil.ok();
	}

	/**
	 * 删除菜单
	 */
	@PreAuthorize("@ss.hasPermi('system:menu:remove')")
	@DeleteMapping("/{menuId}")
	public Object remove(@PathVariable("menuId") Long menuId) {
		sysMenuService.deleteMenuById(menuId);
		return ResultUtil.ok();
	}

}
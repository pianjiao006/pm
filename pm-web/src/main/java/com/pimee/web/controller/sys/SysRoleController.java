package com.pimee.web.controller.sys;

import com.github.pagehelper.PageInfo;
import com.pimee.common.utils.DateUtil;
import com.pimee.common.utils.ResultUtil;
import com.pimee.common.utils.ServletUtils;
import com.pimee.common.utils.StringUtils;
import com.pimee.common.utils.security.LoginUser;
import com.pimee.common.utils.security.SecurityUtils;
import com.pimee.model.SysRole;
import com.pimee.service.sys.ISysRoleService;
import com.pimee.service.sys.impl.SysUserService;
import com.pimee.support.security.service.PermissionService;
import com.pimee.support.security.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/system/role")
public class SysRoleController {

	@Resource
	private ISysRoleService sysRoleService;
	@Resource
	private TokenService tokenService;
	@Resource
	private SysUserService sysUserService;
	@Resource
	private PermissionService permissionService;

	@PreAuthorize("@ss.hasPermi('system:role:list')")
	@GetMapping("/list")
	public Object list(Map<String, Object> params) {
		// 获取当前租户
		Long tenantId = sysUserService.getTenantId(SecurityUtils.getUserId());
		params.put("tenantId", tenantId);
		PageInfo<SysRole> pageUser = sysRoleService.pageList(params);
		return ResultUtil.ok(pageUser);
	}

	/**
	 * 根据角色编号获取详细信息
	 */
	@PreAuthorize("@ss.hasPermi('system:role:query')")
	@GetMapping(value = "/{roleId}")
	public Object getInfo(@PathVariable Long roleId) {
		SysRole sysRole = sysRoleService.selectByKey(roleId);
		return ResultUtil.ok(sysRole);
	}

	/**
	 * 新增角色
	 */
	@PreAuthorize("@ss.hasPermi('system:role:add')")
	@PostMapping
	public Object add(@Validated @RequestBody SysRole role) {
		role.setCreateBy(SecurityUtils.getUsername());
		role.setCreateTime(DateUtil.now());
		sysRoleService.insertRole(role);
		return ResultUtil.ok();

	}

	/**
	 * 修改保存角色
	 */
	@PreAuthorize("@ss.hasPermi('system:role:edit')")
	@PutMapping
	public Object edit(@Validated @RequestBody SysRole role) {
		role.setUpdateBy(SecurityUtils.getUsername());
		role.setUpdateTime(DateUtil.now());
		sysRoleService.updateRole(role);
		// 更新缓存用户权限
		LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
		if (StringUtils.isNotNull(loginUser.getUser()) && !loginUser.getUser().isAdmin()) {
			loginUser.setPermissions(permissionService.getMenuPermission(loginUser.getUser()));
			loginUser.setUser(sysUserService.selectUserByUserName(loginUser.getUser().getUserName()));
			tokenService.setLoginUser(loginUser);
		}
		return ResultUtil.ok();
	}

	/**
	 * 修改保存数据权限
	 */
	@PreAuthorize("@ss.hasPermi('system:role:edit')")
	@PutMapping("/dataScope")
	public Object dataScope(@RequestBody SysRole role) {
		sysRoleService.checkRoleAllowed(role);
		sysRoleService.authDataScope(role);
		return ResultUtil.ok();
	}

	/**
	 * 状态修改
	 */
	@PreAuthorize("@ss.hasPermi('system:role:edit')")
	@PutMapping("/changeStatus")
	public Object changeStatus(@RequestBody SysRole role) {
		sysRoleService.checkRoleAllowed(role);
		role.setUpdateBy(SecurityUtils.getUsername());
		sysRoleService.updateNotNull(role);
		return ResultUtil.ok();
	}

	/**
	 * 删除角色
	 */
	@PreAuthorize("@ss.hasPermi('system:role:remove')")
	@DeleteMapping("/{roleIds}")
	public Object remove(@PathVariable Long[] roleIds) {
		sysRoleService.deleteRoleByKeys(roleIds);
		return ResultUtil.ok();
	}

	/**
	 * 获取角色选择框列表
	 */
	@PreAuthorize("@ss.hasPermi('system:role:query')")
	@GetMapping("/optionselect")
	public Object optionselect() {
		sysRoleService.listAll();
		return ResultUtil.ok();
	}

}

package com.pimee.web.controller.monitor;

import com.pimee.common.constants.Constants;
import com.pimee.common.utils.RedisUtil;
import com.pimee.common.utils.ResultUtil;
import com.pimee.common.utils.StringUtils;
import com.pimee.common.utils.security.LoginUser;
import com.pimee.model.SysUserOnline;
import com.pimee.service.sys.ISysUserOnlineService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * 在线用户监控
 *
 * @author Bruce Shaw
 */
@RestController
@RequestMapping("/monitor/online")
public class SysUserOnlineController {
    @Resource
    private ISysUserOnlineService sysUserOnlineService;
    @Resource
    private RedisUtil<LoginUser> redisUtil;

    /**
     * 获取租户分页列表
     */
    @PreAuthorize("@ss.hasPermi('system:online:list')")
    @GetMapping("/list")
    public Object pageList(String ipaddr, String userName) {
        Collection<String> keys = redisUtil.keys(Constants.LOGIN_TOKEN_KEY + "*");
        List<SysUserOnline> userOnlineList = new ArrayList<>();
        for (String key : keys) {
            LoginUser user = redisUtil.string_get(key);
            if (StringUtils.isNotEmpty(ipaddr) && StringUtils.isNotEmpty(userName)) {
                if (StringUtils.equals(ipaddr, user.getIpaddr()) && StringUtils.equals(userName, user.getUsername())) {
                    userOnlineList.add(sysUserOnlineService.selectOnlineByInfo(ipaddr, userName, user));
                }
            } else if (StringUtils.isNotEmpty(ipaddr)) {
                if (StringUtils.equals(ipaddr, user.getIpaddr())) {
                    userOnlineList.add(sysUserOnlineService.selectOnlineByIpaddr(ipaddr, user));
                }
            } else if (StringUtils.isNotEmpty(userName) && StringUtils.isNotNull(user.getUser())) {
                if (StringUtils.equals(userName, user.getUsername())) {
                    userOnlineList.add(sysUserOnlineService.selectOnlineByUserName(userName, user));
                }
            } else {
                userOnlineList.add(sysUserOnlineService.loginUserToUserOnline(user));
            }
        }
        Collections.reverse(userOnlineList);
        userOnlineList.removeAll(Collections.singleton(null));

        return ResultUtil.okPageList(userOnlineList);
    }

    /**
     * 强退用户
     */
    @PreAuthorize("@ss.hasPermi('monitor:online:forceLogout')")
//    @Log(title = "在线用户强制退出", businessType = BusinessType.FORCE)
    @DeleteMapping("/{tokenId}")
    public Object forceLogout(@PathVariable String tokenId) {
        redisUtil.delete(Constants.LOGIN_TOKEN_KEY + tokenId);
        return ResultUtil.ok();
    }

}

package com.pimee.web.controller.exception;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.pimee.common.core.support.HttpCode;
import com.pimee.common.exception.BusinessException;
import com.pimee.common.exception.IllegalParameterException;
import com.pimee.common.exception.UserPasswordNotMatchException;
import com.pimee.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 全局异常处理器
 * 
 * @author Bruce Shaw 2020年4月14日 上午11:53:37
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	/** 异常处理 */
	@ExceptionHandler(Exception.class)
	public void exceptionHandler(HttpServletRequest request, HttpServletResponse response, Exception ex)
			throws Exception {
		logger.error("", ex);
		ModelMap modelMap = new ModelMap();
		if (ex instanceof BusinessException) {
			BusinessException busiException = (BusinessException) ex;
			modelMap.put("code", busiException.getCode());
			modelMap.put("msg", busiException.getMsg());
		} else if (ex instanceof IllegalArgumentException) {
			IllegalParameterException paramException = (IllegalParameterException) ex;
			modelMap.put("code", paramException.getCode());
			modelMap.put("msg", paramException.getMsg());
		} else if (ex instanceof UserPasswordNotMatchException) {
			UserPasswordNotMatchException paramException = (UserPasswordNotMatchException) ex;
			modelMap.put("code", paramException.getCode());
			modelMap.put("msg", paramException.getMsg());
		} else if (ex instanceof DuplicateKeyException) {
			modelMap.put("code", HttpCode.INTERNAL_SERVER_ERROR.value().toString());
			modelMap.put("msg", "存在相同的记录");
		} else {
			modelMap.put("code", HttpCode.INTERNAL_SERVER_ERROR.value().toString());
			String msg = StringUtils.defaultIfBlank(ex.getMessage(), HttpCode.INTERNAL_SERVER_ERROR.msg());
			modelMap.put("msg", msg.length() > 100 ? "系统异常,请稍候再试." : msg);
		}
		response.setContentType("application/json;charset=UTF-8");
		modelMap.put("timestamp", System.currentTimeMillis());
		logger.info("===> exceptionHandler RESPONSE : " + JSON.toJSON(modelMap));
		byte[] bytes = JSON.toJSONBytes(modelMap, SerializerFeature.DisableCircularReferenceDetect);
		response.getOutputStream().write(bytes);
	}
}

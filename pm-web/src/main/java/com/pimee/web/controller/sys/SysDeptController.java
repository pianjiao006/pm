package com.pimee.web.controller.sys;

import com.google.common.collect.Maps;
import com.pimee.common.utils.ResultUtil;
import com.pimee.common.utils.StringUtils;
import com.pimee.common.utils.security.SecurityUtils;
import com.pimee.model.SysDept;
import com.pimee.model.vo.TreeSelect;
import com.pimee.service.sys.ISysDeptService;
import com.pimee.web.controller.model.AjaxResult;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 代理管理
 */
@RestController
@RequestMapping("/system/dept")
public class SysDeptController {
    @Resource
    private ISysDeptService sysDeptService;

    /**
     * 获取部门列表
     */
    @PreAuthorize("@ss.hasPermi('system:dept:list')")
    @GetMapping("/list")
    public Object list(@RequestParam Map<String, Object> params) {
        List<SysDept> deptList = sysDeptService.listDept(params);
        return ResultUtil.ok(deptList);
    }

    /**
     * 根据部门编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:dept:query')")
    @GetMapping(value = "/{deptId}")
    public Object getInfo(@PathVariable Long deptId) {
        return ResultUtil.ok(sysDeptService.selectByKey(deptId));
    }

    /**
     * 查询部门列表（排除节点）
     */
    @PreAuthorize("@ss.hasPermi('system:dept:list')")
    @GetMapping("/list/exclude/{deptId}")
    public Object excludeChild(@PathVariable(value = "deptId", required = false) Long deptId) {
        List<SysDept> deptList = sysDeptService.listDept(Maps.newHashMap());
        Iterator<SysDept> it = deptList.iterator();
        while (it.hasNext()) {
            SysDept d = it.next();
            if (d.getDeptId().intValue() == deptId
                    || ArrayUtils.contains(StringUtils.split(d.getAncestors(), ","), deptId + "")) {
                it.remove();
            }
        }
        return ResultUtil.ok(deptList);
    }

    /**
     * 获取部门下拉树列表
     */
    @GetMapping("/treeSelect")
    public Object treeSelect(SysDept dept) {
        List<TreeSelect> treeList = sysDeptService.buildDeptTreeSelect();
        return ResultUtil.ok(treeList);
    }

    /**
     * 加载对应角色部门列表树
     */
    @GetMapping(value = "/roleDeptTreeSelect/{roleId}")
    public Object roleDeptTreeSelect(@PathVariable("roleId") Long roleId) {
        AjaxResult ajax = AjaxResult.success();
        ajax.put("checkedKeys", sysDeptService.selectDeptListByRoleId(roleId));
        ajax.put("depts", sysDeptService.buildDeptTreeSelect());
        return ajax;
    }

    /**
     * 新增部门
     */
    @PreAuthorize("@ss.hasPermi('system:dept:add')")
    @PostMapping
    public Object add(@Validated @RequestBody SysDept dept) {
        dept.setCreateBy(SecurityUtils.getUsername());
        sysDeptService.insertDept(dept);
        return ResultUtil.ok();
    }

    /**
     * 修改部门
     */
    @PreAuthorize("@ss.hasPermi('system:dept:edit')")
    @PutMapping
    public Object edit(@Validated @RequestBody SysDept dept) {
        dept.setUpdateBy(SecurityUtils.getUsername());
        sysDeptService.updateDept(dept);
        return ResultUtil.ok();
    }

    /**
     * 删除部门
     */
    @PreAuthorize("@ss.hasPermi('system:dept:remove')")
    @DeleteMapping("/{deptId}")
    public Object remove(@PathVariable Long deptId) {
        sysDeptService.deleteDeptById(deptId);
        return ResultUtil.ok();
    }
}

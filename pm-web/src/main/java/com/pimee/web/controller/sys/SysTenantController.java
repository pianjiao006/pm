package com.pimee.web.controller.sys;

import com.github.pagehelper.PageInfo;
import com.pimee.common.aop.annotation.Log;
import com.pimee.common.aop.enums.BusinessType;
import com.pimee.common.utils.ResultUtil;
import com.pimee.common.utils.security.SecurityUtils;
import com.pimee.model.SysTenant;
import com.pimee.model.vo.SysTenantVo;
import com.pimee.service.sys.ISysTenantService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 租户信息操作处理
 *
 * @author Bruce Shaw
 */
@RestController
@RequestMapping("/system/tenant")
public class SysTenantController {
    @Resource
    private ISysTenantService sysTenantService;

    /**
     * 获取租户分页列表
     */
    @PreAuthorize("@ss.hasPermi('system:tenant:list')")
    @GetMapping("/list")
    public Object pageList(@RequestParam Map<String, Object> params) {
        PageInfo<Map<String, Object>> pageList = sysTenantService.pageList(params);
        return ResultUtil.ok(pageList);
    }
    /**
     * 获取租户列表
     */
    @PreAuthorize("@ss.hasPermi('system:tenant:list')")
    @GetMapping("/listAll")
    public Object listAll() {
        List<SysTenant> listTenant = sysTenantService.listTenant();
        return ResultUtil.ok(listTenant);
    }

    /**
     * 根据租户编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tenant:query')")
    @GetMapping(value = "/{tenantId}")
    public Object getInfo(@PathVariable Long tenantId) {
        return ResultUtil.ok(sysTenantService.selectByKey(tenantId));
    }

    /**
     * 新增租户
     */
    @Log(title = "新增租户", businessType = BusinessType.INSERT)
    @PreAuthorize("@ss.hasPermi('system:tenant:add')")
    @PostMapping
    public Object add(@Validated @RequestBody SysTenant param) {
        String userName = SecurityUtils.getUsername();
        param.setCreateBy(userName);
        param.setUpdateBy(userName);
        sysTenantService.insert(param);
        return ResultUtil.ok();
    }

    /**
     * 修改租户
     */
    @Log(title = "修改租户", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('system:tenant:edit')")
    @PutMapping
    public Object edit(@Validated @RequestBody SysTenant param) {
        param.setUpdateBy(SecurityUtils.getUsername());
        sysTenantService.update(param);
        return ResultUtil.ok();
    }

    /**
     * 租户续费
     */
    @Log(title = "租户续费", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('system:tenant:edit')")
    @PostMapping("renew")
    public Object renew(@Validated @RequestBody SysTenantVo param) {
        param.setUpdateBy(SecurityUtils.getUsername());
        sysTenantService.renew(param);
        return ResultUtil.ok();
    }

    /**
     * 删除租户
     */
    @Log(title = "删除租户", businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermi('system:tenant:remove')")
    @DeleteMapping("/{tenantIds}")
    public Object remove(@PathVariable Long[] tenantIds) {
        sysTenantService.deleteByIds(tenantIds);
        return ResultUtil.ok();
    }

}

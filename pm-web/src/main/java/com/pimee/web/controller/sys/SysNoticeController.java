package com.pimee.web.controller.sys;

import com.github.pagehelper.PageInfo;
import com.pimee.common.utils.ResultUtil;
import com.pimee.common.utils.security.SecurityUtils;
import com.pimee.model.SysNotice;
import com.pimee.service.sys.ISysNoticeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 公告 信息操作处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/notice")
public class SysNoticeController {
    @Resource
    private ISysNoticeService sysNoticeService;

    /**
     * 获取参数配置分页列表
     */
    @PreAuthorize("@ss.hasPermi('system:notice:list')")
    @GetMapping("/list")
    public Object list(@RequestParam Map<String, Object> params) {
        PageInfo<SysNotice> pageResult = sysNoticeService.pageList(params);
        return ResultUtil.ok(pageResult);
    }

    /**
     * 根据参数配置编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:notice:query')")
    @GetMapping(value = "/{noticeId}")
    public Object getInfo(@PathVariable Long noticeId) {
        return ResultUtil.ok(sysNoticeService.selectByKey(noticeId));
    }

    /**
     * 新增参数配置
     */
    @PreAuthorize("@ss.hasPermi('system:notice:add')")
    @PostMapping
    public Object add(@Validated @RequestBody SysNotice notice) {
        notice.setCreateBy(SecurityUtils.getUsername());
        sysNoticeService.insertNotice(notice);
        return ResultUtil.ok();
    }

    /**
     * 修改参数配置
     */
    @PreAuthorize("@ss.hasPermi('system:notice:edit')")
    @PutMapping
    public Object edit(@Validated @RequestBody SysNotice notice) {
        notice.setUpdateBy(SecurityUtils.getUsername());
        sysNoticeService.updateNotice(notice);
        return ResultUtil.ok();
    }

    /**
     * 删除参数配置
     */
    @PreAuthorize("@ss.hasPermi('system:notice:remove')")
    @DeleteMapping("/{noticeIds}")
    public Object remove(@PathVariable Long[] noticeIds) {
        sysNoticeService.deleteByIds(noticeIds);
        return ResultUtil.ok();
    }
}

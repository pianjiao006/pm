package com.pimee.web.controller.sys;

import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.pimee.common.constants.UserConstants;
import com.pimee.common.utils.ResultUtil;
import com.pimee.common.utils.StringUtils;
import com.pimee.common.utils.security.LoginUser;
import com.pimee.common.utils.security.SecurityUtils;
import com.pimee.model.SysRole;
import com.pimee.model.SysUser;
import com.pimee.model.vo.SysUserVo;
import com.pimee.service.sys.ISysPostService;
import com.pimee.service.sys.ISysRoleService;
import com.pimee.service.sys.ISysUserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/system/user")
public class SysUserController {
	@Resource
	private ISysUserService sysUserService;
	@Resource
	private ISysRoleService sysRoleService;
	@Resource
	private ISysPostService sysPostService;

	/**
	 * 获取用户列表
	 */
	@PreAuthorize("@ss.hasPermi('system:user:list')")
	@GetMapping("/list")
	public Object list(@RequestParam Map<String, Object> params) {
		// 兼容用户列表在点击代理可以过滤
		if(!SysUser.isAdmin(SecurityUtils.getUserId()) && params.get("deptId") == null){
			LoginUser loginUser = SecurityUtils.getLoginUser();
			Long deptId = loginUser.getUser().getDeptId();
			params.put("deptId", deptId);
		}
		PageInfo<SysUserVo> pageList = sysUserService.pageList(params);
		return ResultUtil.ok(pageList);
	}

	/**
	 * 根据用户编号获取详细信息
	 */
	@PreAuthorize("@ss.hasPermi('system:user:query')")
	@GetMapping(value = { "/", "/{userId}" })
	public Object getInfo(@PathVariable(value = "userId", required = false) Long userId) {
		Map<String, Object> ajax = Maps.newHashMap();
		List<SysRole> roles = sysRoleService.listAll();
		ajax.put("roles", SysUser.isAdmin(userId) ? roles
				: roles.stream().filter(r -> !r.isAdmin()).collect(Collectors.toList()));
		ajax.put("posts", sysPostService.listAll());
		if (StringUtils.isNotNull(userId)) {
			ajax.put("data", sysUserService.selectUserById(userId));
			ajax.put("postIds", sysPostService.selectPostIdsByUserId(userId));
			List<Long> roleIds = sysRoleService.selectRoleIdsByUserId(userId);
			ajax.put("roleIds", roleIds);
		}
		return ajax;
	}

	/**
	 * 新增用户
	 */
	@PreAuthorize("@ss.hasPermi('system:user:add')")
	@PostMapping
	public Object add(@Validated @RequestBody SysUserVo user) {
		if (UserConstants.NOT_UNIQUE.equals(sysUserService.checkLoginNameUnique(user.getUserName()))) {
			return ResultUtil.error("新增用户'" + user.getUserName() + "'失败，登录账号已存在");
		} else if (UserConstants.NOT_UNIQUE.equals(sysUserService.checkPhoneUnique(user.getPhonenumber()))) {
			return ResultUtil.error("新增用户'" + user.getUserName() + "'失败，手机号码已存在");
		}
		user.setCreateBy(SecurityUtils.getUsername());
		user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
		sysUserService.addUser(user);
		return ResultUtil.ok();
	}

	/**
	 * 修改用户
	 */
	@PreAuthorize("@ss.hasPermi('system:user:edit')")
	@PutMapping
	public Object edit(@Validated @RequestBody SysUserVo user) {
		user.setUpdateBy(SecurityUtils.getUsername());
		sysUserService.updateUser(user);
		return ResultUtil.ok();
	}

	/**
	 * 删除用户
	 */
	@PreAuthorize("@ss.hasPermi('system:user:remove')")
	@DeleteMapping("/{userIds}")
	public Object remove(@PathVariable Long[] userIds) {
		sysUserService.deleteUserByIds(userIds);
		return ResultUtil.ok();
	}

	/**
	 * 重置密码
	 */
	@PreAuthorize("@ss.hasPermi('system:user:resetPwd')")
	@PutMapping("/resetPwd")
	public Object resetPwd(@RequestBody SysUser user) {
		user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
		user.setUpdateBy(SecurityUtils.getUsername());
		sysUserService.resetUserPwd(user);
		return ResultUtil.ok();
	}

	/**
	 * 状态修改
	 */
	@PreAuthorize("@ss.hasPermi('system:user:edit')")
	@PutMapping("/changeStatus")
	public Object changeStatus(@RequestBody SysUserVo user) {
		user.setUpdateBy(SecurityUtils.getUsername());
		sysUserService.changeStatus(user);
		return ResultUtil.ok();
	}

}
